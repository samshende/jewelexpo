import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Picker,
    Dimensions,
    TouchableWithoutFeedback,
    PermissionsAndroid,
    AsyncStorage
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import Icon1 from 'react-native-vector-icons/AntDesign'
import ImagePicker from 'react-native-image-picker';
import createAppointment from '../GraphQl/createAppointment'
import getJewelExhibitors from '../GraphQl/getJewelExhibitors'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
    Item, Input, Icon, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast,Tab,Tabs
} from 'native-base';
import BookedApp from './BookedApp'
import Filter from '../Public/icon/Filter.svg'
const requestCameraPermission = async () => {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
                title: "Cool Photo App Camera Permission",
                message:
                    "Cool Photo App needs access to your camera " +
                    "so you can take awesome pictures.",
                buttonNeutral: "Ask Me Later",
                buttonNegative: "Cancel",
                buttonPositive: "OK"
            }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the camera");
        } else {
            console.log("Camera permission denied");
        }
    } catch (err) {
        console.log(err);
    }
};


class Appointment extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: 'java',
            userName: '',
            error: false,
            city: '',
            Enquiry: '',
            number: '',
            address: '',
            companyName: '',
            profile1: '',
            profilePhoto: '',
            proFileName: '',
            FirstSelectname: 'AM',
            isConnected: true,
            fName: '',
            lName: '',
            dob: '',
            aTitle: '',
            aTime: '',
            selNameData: [],
            nameval: '',
            loading: false,
            exhNumber: '',
            arr: [
                {
                    name: 'Gold Chain',
                    val: "GOLD_CHAINS",
                    check: false,
                    id: 1
                },
                {
                    name: 'Bangles',
                    val: "BANGLES",
                    check: false,
                    id: 2
                },
                {
                    name: 'Bracelets',
                    val: "BRACELETS",
                    check: false,
                    id: 3
                },
                {
                    name: 'Temple jewellery',
                    val: "TEMPLE_JEWELLERY",
                    check: false,
                    id: 4
                },
                {
                    name: 'Mangalsutras',
                    val: "MANGALSUTRAS",
                    check: false,
                    id: 5,

                },
                {
                    name: 'jewellery',//Lightweight
                    val: "LIGHTWEIGHT_JEWELLERY",
                    check: false,
                    id: 6
                },
                {
                    name: 'Casting jewellery',
                    val: "CASTING_JEWELLERY",
                    check: false,
                    id: 7
                },
                {
                    name: 'Antique jewellery',
                    val: "ANTIQUE_JEWELLERY",
                    check: true,
                    id: 8
                },
                {
                    name: 'Diamond jewellery',
                    val: "DIAMOND_JEWELLERY",
                    check: false,
                    id: 9
                },
                {
                    name: 'Turkish jewellery',
                    val: "TURKISH_JEWELLERY",
                    check: false,
                    id: 10
                },
                {
                    name: 'Silver jewellery',
                    val: "SILVER_JEWELLERY",
                    check: true,
                    id: 11
                },
                {
                    name: 'Plain gold jewellery',
                    val: "PLAIN_GOLD_JEWELLERY",
                    check: false,
                    id: 12
                },
                {
                    name: 'Silver articles',
                    val: "SILVER_ARTICLES",
                    check: false,
                    id: 13
                },
                {
                    name: 'Gold Mountings',
                    val: "GOLD_MOUNTINGS",
                    check: false,
                    id: 14
                },
                {
                    name: 'Gold jewellery',
                    val: "GOLD_JEWELLERY",
                    check: false,
                    id: 15
                },
                {
                    name: 'Loose  Diamonds',
                    val: "LOOSE_DIAMONDS",
                    check: true,
                    id: 16
                },
                {
                    name: 'Nose Pins',
                    val: "NOSE_PINS",
                    check: false,
                    id: 17
                },
                {
                    name: 'Jadau jewellery',
                    val: "JADAU_JEWELLERY",
                    check: false,
                    id: 18
                },
            ]
        };
    }

    oncityChange = (value) => {
        let num = '', name = '';

        this.state.selNameData.map((i, j) => {
            if (i.value == value) {
                name = i.key
            }
        })

        this.state.data.map((i, j) => {
            if (i.companyName == name) {
                num = i.id
            }
        })

        console.log('id', num, name)
        this.setState({
            exhCompany: name,
            exhNumber: num,
            nameval: value
        });
    }

    getBlob = response => {
        console.log('in getBlob ::', response);
        const xhr = new XMLHttpRequest();
        xhr.open('GET', response.uri, true);
        xhr.responseType = 'blob';
        xhr.onload = () => {
            const reader = new FileReader();
            reader.onloadend = () => {
                let myBase64 = reader.result;
                var buf = new Buffer(
                    myBase64.replace(/^data:image\/\w+;base64,/, ''),
                    'base64',
                );

                this.setState({
                    profilePhoto: buf,
                    proFileName: response.fileName,
                });
            };
            reader.readAsDataURL(xhr.response);
        };
        xhr.send();
    };

    delete = () => {
        this.setState({
            proFileName: '',
            profile1: '',
            profilePhoto: '',
        });
    };
    selectPhotoTapped() {
        requestCameraPermission()
        const options = {
            quality: 1.0,
            maxWidth: 500,
            maxHeight: 500,
            title: 'Select Photo',
            storageOptions: {
                skipBackup: true,
            },
        };

        ImagePicker.showImagePicker(options, response => {
            console.log('Response showImagePicker = ', response);
            if (response.didCancel) {
                console.log('User cancelled photo picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else {
                let source = { uri: response.uri, fileName: response.fileName };
                console.log("source", source)
                console.log("source", `data:image/png;base64,${response.data}`)
                this.getBlob(source);
                this.setState({
                    profile1: `data:image/png;base64,${response.data}`,
                });
            }
        });
    }

    createDatasource = (data) => {
        let name = []
        if (data != null) {
            data.map((i, j) => {

                let obj2 = {
                    key: i.companyName,
                    value: j,
                    id: i.id

                }
                name.push(obj2)
            })

        }
        var finalsort1 = name.sort((a, b) => a.key.toLowerCase() > b.key.toLowerCase() ? 1 : -1)
        console.log('data source', finalsort1)

        this.setState({
            selNameData: finalsort1,
            nameval: finalsort1[0].value,
            exhNumber: finalsort1[0].id,
            exhCompany: finalsort1[0].key
        })
    }

    runQuery = () => {
        try {
            this.props.client
                .query({
                    query: getJewelExhibitors,
                    fetchPolicy: 'no-cache',
                    variables: { exhName: 'jewExpo' },
                })
                .then(({ data }) => {
                    console.log("data", data)
                    this.setState({
                        data: data.getJewelExhibitors,
                        loading: false,
                    }, () => { this.createDatasource(data.getJewelExhibitors) })
                })
                .catch(err => {
                    console.log(err);
                });
        } catch (err) {
            console.log('iN catfch', err);
        }
    }


    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });

        this.runQuery()

        AsyncStorage.getItem('user')
            .then(data => {
                let data1 = JSON.parse(data);
                if (data1 != null) {
                    this.setState({
                        fName: data1.basicVisitor && data1.basicVisitor.fName ? data1.basicVisitor.fName : '',
                        lName: data1.basicVisitor && data1.basicVisitor.lName ? data1.basicVisitor.lName : '',
                        companyName: data1.basicVisitor && data1.basicVisitor.companyName ? data1.basicVisitor.companyName : '',
                        number: data1.id,
                        city: data1.basicVisitor && data1.basicVisitor.address && data1.basicVisitor.address.city ? data1.basicVisitor.address.city : '',
                        address: data1.basicVisitor && data1.basicVisitor.email ? data1.basicVisitor.email : '',
                    })
                } else {
                }
            })
            .catch(err => console.log('Error in login page', err));
    }

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }
    enterLoading = () => {
        this.setState({
            loading: true
        })
    }
    handleSubmit = () => {
        this.enterLoading()
        if (this.state.companyName == '' || this.state.fName == '' || this.state.address == '' || this.state.number == '' || this.state.Enquiry == '' || this.state.city == '' ||
            this.state.lName == '' || this.state.aTime == '' || this.state.aTitle == '') {
            this.setState({
                error: true,
                loading: false
            })
            return
        }
        else {
            console.log(this.state)
            //             date: Math.floor(date.valueOf()/1000)
            let productsInterestedIn = []
            this.state.arr.map((i) => {
                if (i.check == true) {
                    productsInterestedIn.push(i.val)
                }
            })
            console.log('piiiii.....', productsInterestedIn)


            this.props.client
                .mutate({
                    mutation: createAppointment,
                    variables: {
                        userMob: this.state.number,
                        exhNumber: this.state.exhNumber,
                        email: this.state.address,
                        fName: this.state.fName,
                        lName: this.state.lName,
                        exhCompany: this.state.exhCompany,
                        city: this.state.city,
                        status: 'new',
                        title: this.state.aTitle,
                        notes: this.state.Enquiry,
                        appDate: this.state.dob,
                        companyName: this.state.companyName,
                        productInterestedIn: productsInterestedIn.length > 0 ? productsInterestedIn : undefined
                    },
                })
                .then(({ data }) => {
                    console.log("data", data)
                    this.setState({
                        loading: false
                    })
                    Toast.show({
                        text: 'Meeting has been requested, you will be updated via sms',
                        type: 'success',
                        position: 'center',
                        duration: 5000,
                        style: { alignSelf: 'center', width: wp('70%'), height: "auto", borderRadius: 10, marginTop: 20 },
                        textStyle: {
                            textAlign: 'center',
                            color: 'black'
                        }
                    });
                })
                .catch(err => {
                    this.setState({
                        loading: false
                    })
                    console.log(`generateOtp : in err: ${JSON.stringify(err)}`);
                });

        }
    }
    handleDate = (data) => {
        console.log(data)
        let date = Math.floor(data.valueOf() / 1000)
        console.log('d', date)
        this.setState({
            dob: date
        })
    }

    toggleCheckbox = (id) => {
        console.log("id", id)
        let changedCheckbox = ''
        changedCheckbox = this.state.arr.find((cb) => cb.id === id);
        changedCheckbox.check = !changedCheckbox.check;
        console.log("c", changedCheckbox)
        const arr = Object.assign([], this.state.arr, changedCheckbox);
        this.setState({ arr });
    }

    render() {
        console.log("exhnum", this.state.exhNumber)
        console.log("name", this.state.exhCompany)

        const minDate = new Date();
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }

        return (

            <Container style={{ backgroundColor: '#e5e5e5' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 50 }}>
                    <Icon1 onPress={() => { this.props.navigation.navigate('Dashbord') }} style={{ width: wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                    <Text style={{ fontSize: 24, textAlign: 'center', width: wp('80%'), alignSelf: 'center', }}>Manage Appointments</Text>
                </View>

                {/* <Header searchBar rounded style={{ backgroundColor: '#e5e5e5', elevation: 0 }}>
          <Item>
            <Icon name="ios-search" />
            <Input placeholder="Search here...." />
            <Filter style={{ marginRight: 10 }} />
          </Item>
        </Header> */}
               <Tabs >
                    <Tab
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ color: 'black', fontWeight: '500' }}
                        activeTextStyle={{ color: '#C4941C', fontWeight: '500' }}
                        heading="New Appointment"
                    >

                {this.state.loading ?
                    <Spinner style={{ alignSelf: 'center', flex: 1 }} color='blue' />
                    :
            <Container style={{ backgroundColor: '#e5e5e5' }}>
                    <Content>
                        <View style={{ margin: 15 }}>
                            <Form>

                                <Label style={{ marginBottom: 7, marginTop: 15 }}>Select Exhibitor<Text style={{ color: "red" }}>*</Text></Label>
                                <Picker
                                    mode="dropdown"
                                    style={{ width: wp('91%'), backgroundColor: 'white' }}
                                    placeholder="Select Exhibitor"
                                    placeholderStyle={{ color: '#bfc6ea' }}
                                    placeholderIconColor="#007aff"
                                    selectedValue={this.state.nameval}
                                    onValueChange={this.oncityChange.bind(this)}
                                >
                                    {this.state.selNameData.length != 0 && this.state.selNameData.map((i, j) => {
                                        return (
                                            <Picker.Item label={i.key} value={i.value} />
                                        )
                                    })}
                                </Picker>

                                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10, marginTop: 15, width: wp('91%') }}>
                                    <View>
                                        <Label style={{ marginBottom: 7 }}>First Name<Text style={{ color: "red" }}>*</Text></Label>
                                        <Input style={{ backgroundColor: 'white', width: wp('45%') }}
                                            placeholder='first name'
                                            value={this.state.fName}
                                            onChangeText={data =>
                                                this.setState({ fName: data, error: false })
                                            }
                                        />
                                    </View>
                                    <View>
                                        <Label style={{ marginBottom: 7 }}>Last Name<Text style={{ color: "red" }}>*</Text></Label>
                                        <Input style={{ backgroundColor: 'white', width: wp('45%') }}
                                            placeholder='last name'
                                            value={this.state.lName}
                                            onChangeText={data =>
                                                this.setState({ lName: data, error: false })
                                            }
                                        />
                                    </View>

                                </View>
                                {this.state.error && this.state.fName == '' && this.state.lName == '' ?
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter  Name...{' '}
                                    </Text>
                                    : <View></View>
                                }
                                <Label style={{ marginBottom: 7 }}>Company Name<Text style={{ color: "red" }}>*</Text></Label>
                                <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                    placeholder='company name'
                                    value={this.state.companyName}
                                    onChangeText={data =>
                                        this.setState({ companyName: data, error: false })
                                    }
                                />
                                {this.state.error && this.state.companyName == '' &&
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter Company Name...{' '}
                                    </Text>
                                }

                                <Label style={{ marginBottom: 7, marginTop: 15 }}>Email Address<Text style={{ color: "red" }}>*</Text></Label>
                                <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                    placeholder='email'
                                    value={this.state.address}
                                    onChangeText={data =>
                                        this.setState({ address: data, error: false })
                                    }
                                />
                                {this.state.error && this.state.address == '' &&
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter Email Address...{' '}
                                    </Text>
                                }


                                <Label style={{ marginBottom: 7, marginTop: 15 }}>Whatsapp Number<Text style={{ color: "red" }}>*</Text></Label>
                                <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                    placeholder='number'
                                    value={this.state.number}
                                    pattern="[1-9]{1}[0-9]{9}"
                                    maxLength={10}
                                    onChangeText={data =>
                                        this.setState({ number: data, error: false })
                                    }
                                />
                                {this.state.error && this.state.number == '' &&
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter Phone Number...{' '}
                                    </Text>
                                }


                                <Label style={{ marginBottom: 7, marginTop: 15 }}>City<Text style={{ color: "red" }}>*</Text></Label>
                                <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                    placeholder='city'
                                    value={this.state.city}
                                    onChangeText={data =>
                                        this.setState({ city: data, error: false })
                                    }
                                />
                                {this.state.error && this.state.city == '' &&
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter City...{' '}
                                    </Text>
                                }

                                <Label style={{ marginBottom: 7, marginTop: 15 }}>Appointment Title<Text style={{ color: "red" }}>*</Text></Label>
                                <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                    placeholder='Appointment Title'
                                    value={this.state.aTitle}
                                    onChangeText={data =>
                                        this.setState({ aTitle: data, error: false })
                                    }
                                />
                                {this.state.error && this.state.aTitle == '' &&
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter Appointment Title...{' '}
                                    </Text>
                                }

                                <Label style={{ marginBottom: 7, marginTop: 15 }}>Appointment Date<Text style={{ color: "red" }}>*</Text></Label>
                                <Item style={{ width: wp('91%'), backgroundColor: 'white', marginLeft: -2, height: 50 }}>
                                    <DatePicker
                                        style={{}}
                                        minimumDate={minDate}
                                        locale={'en'}
                                        timeZoneOffsetInMinutes={undefined}
                                        modalTransparent={false}
                                        animationType={'fade'}
                                        androidMode={'calendar'}
                                        textStyle={{ color: "black" }}
                                        placeHolderText="Select date                                                                                       "
                                        placeHolderTextStyle={{ color: "#d3d3d3" }}
                                        onDateChange={this.handleDate}
                                        disabled={false}
                                    />
                                    <Icon style={{ marginRight: 10, position: 'absolute', right: 0 }} light name="calendar" size={20} color={''} />
                                </Item>
                                {this.state.error && this.state.dob == '' &&
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter Appointment Date...{' '}
                                    </Text>
                                }
                                <Label style={{ marginBottom: 7, marginTop: 10 }}>Appointment Time:<Text style={{ color: "red" }}>*</Text></Label>
                                <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                    placeholder='Appointment Time'
                                    value={this.state.aTime}
                                    onChangeText={data =>
                                        this.setState({ aTime: data, error: false })
                                    }
                                />
                                {this.state.error && this.state.aTime == '' &&
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter Appointment Time...{' '}
                                    </Text>
                                }


                                <Label style={{ marginBottom: 7, marginTop: 15 }}>Product interested in<Text style={{ color: "red" }}>*</Text></Label>

                                <View style={{ justifyContent: 'flex-start', flexDirection: 'row', width: wp('91%'), alignItems: 'center', alignContent: 'center', flexWrap: 'wrap' }}>
                                    {this.state.arr.map((i, j) => {
                                        return (
                                            <View style={{ justifyContent: 'flex-start', flexDirection: 'row', width: 160, marginTop: 5, marginBottom: 5 }}>
                                                <CheckBox onPress={() => this.toggleCheckbox(i.id)} style={{ borderRadius: 5 }} checked={i.check} color="#B88A1C" />
                                                <Text style={{ marginLeft: 15, fontWeight: i.check ? 'bold' : 'null' }}>{i.name}</Text>
                                            </View>
                                        )
                                    })}

                                </View>

                                <Label style={{ marginBottom: 7, marginTop: 15 }}>Appointment Notes<Text style={{ color: "red" }}>*</Text></Label>
                                <Textarea rowSpan={5} bordered
                                    style={{ backgroundColor: 'white', width: wp('91%') }}
                                    placeholder='Enquiry Details'
                                    value={this.state.Enquiry}
                                    onChangeText={data =>
                                        this.setState({ Enquiry: data, error: false })
                                    }
                                />
                                {this.state.error && this.state.Enquiry == '' &&
                                    <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                        Please Enter Enquiry Details...{' '}
                                    </Text>
                                }

                                <Label style={{ marginBottom: 7 }}>Upload Images</Label>
                                {this.state.profile1 != '' && (
                                    <Card rounded style={styles.editStyle}>
                                        <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }}>
                                            <Image
                                                source={{ uri: this.state.profile1 }}
                                                style={{ width: 70, height: 80 }}
                                            />
                                            <View
                                                style={{
                                                    position: 'absolute',
                                                    right: 3,
                                                    top: 2,
                                                    height: 20,
                                                    width: 20,
                                                    backgroundColor: '#ccc',
                                                    alignItems: 'center',
                                                    borderRadius: 10,
                                                    justifyContent: 'center',
                                                }}>
                                                <TouchableWithoutFeedback onPress={() => this.delete()}>
                                                    <Text style={{ fontSize: 14 }}>X</Text>
                                                </TouchableWithoutFeedback>
                                            </View>
                                        </View>
                                    </Card>
                                )}
                                {this.state.profile1 == '' && (
                                    <>
                                        <Card style={styles.editStyle}>
                                            <View style={{ justifyContent: 'center', alignItems: 'center', flex: 1 }} >
                                                <View style={{}}>
                                                    <TouchableWithoutFeedback
                                                        onPress={this.selectPhotoTapped.bind(this)}>
                                                        <Text style={styles.fabIcon}>+</Text>
                                                    </TouchableWithoutFeedback>
                                                </View>
                                            </View>
                                        </Card>
                                    </>
                                )}

                                <Button style={{ marginTop: 10, backgroundColor: '#B88A1C' }} full onPress={this.handleSubmit}><Text>Book Appointment</Text></Button>
                            </Form>
                        </View>
                    </Content>
            </Container>
                }
                    </Tab>

                    <Tab
                        tabStyle={{ backgroundColor: 'white' }}
                        activeTabStyle={{ backgroundColor: 'white' }}
                        textStyle={{ color: 'black', fontWeight: '500' }}
                        activeTextStyle={{ color: '#C4941C', fontWeight: '500' }}
                        heading="Booked Appointments "
                    >
                        <BookedApp/>
                        </Tab>

                </Tabs>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        height: 90, width: 90
    },
    viewCardmap: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center',
        alignItems: 'center',
        margin: 10,
        flexWrap: 'wrap',
        display: 'flex',
        flex: 1
    },
    editStyle: {
        backgroundColor: '#fff',
        height: 130,
        width: 120,
        borderRadius: 10
    },
    fabIcon: {
        alignSelf: 'center',
        fontSize: 70,
        color: '#ccc',
    },

    viewText: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center',
        alignItems: 'center',
        marginLeft: 5,
        width: wp('91%')
    },

});


export default withApollo(Appointment)
