import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Picker,
    Modal,
    Alert,
    TouchableWithoutFeedback,
    AsyncStorage
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import MapView from './mapView'
import Icon1 from 'react-native-vector-icons/AntDesign'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Item, Input, Icon,Spinner,Toast
} from 'native-base';
import getJewelExhibitors from '../GraphQl/getJewelExhibitors'
import getProdAccessRequestById from '../GraphQl/getProdAccessRequestById'
import { withApollo, compose, graphql } from 'react-apollo';
import Home from '../Public/icon/Home.svg'
import Enquiry from '../Public/icon/enquiry.svg'
import Notification from '../Public/icon/notification.svg'
import Profile from '../Public/icon/profile.svg'
import Icon2 from '../Public/icon/like_black.svg'
import Message from '../Public/icon/message.svg'
import Filter from '../Public/icon/Filter.svg'
import Callender from '../Public/icon/callender.svg'
import Product from '../Public/icon/Product_view.svg'
import Location from '../Public/icon/location.svg'
import createProdAccessRequest from '../GraphQl/createProdAccessRequest'
class Manufacturers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: 'java',
            loading:true,
            modalVisible: false,
            isConnected: true,
            data: [],
            modalVisible1: false,
            location:{
                latitude: 18.5204,
                longitude: 73.8567,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
            },
            companyName:'',
            city:'',
            selcityData:[],
            selNameData:[],
            cityval:'',
            nameval:'',
            proval:'',
            selProdData:[],
            data1:[],
            applyFilter:false,
            companyNameNew:'',
            randomValue:[],
            ProductsArray:[],
            data11:[],
            dataSourceNonApp:[],
            arr : [
                {
                  name: 'Gold Chain',
                  val:"GOLD_CHAINS",
                  check: false,
                  id:1
                },
                {
                  name: 'Bangles',
                  val:"BANGLES",
                  check: false,
                  id:2
                },
                {
                  name: 'Bracelets',
                  val:"BRACELETS",
                  check: false,
                  id:3
                },
                {
                  name: 'Temple jewellery',
                  val:"TEMPLE_JEWELLERY",
                  check: false,
                  id:4
                },
                {
                  name: 'Mangalsutras',
                  val:"MANGALSUTRAS",
                  check: false,
                  id:5,
              
                },
                {
                  name: 'jewellery',//Lightweight
                  val:"LIGHTWEIGHT_JEWELLERY",
                  check: false,
                  id:6
                },
                {
                  name: 'Casting jewellery',
                  val:"CASTING_JEWELLERY",
                  check: false,
                  id:7
                },
                {
                  name: 'Antique jewellery',
                  val:"ANTIQUE_JEWELLERY",
                  check: true,
                  id:8
                },
                {
                  name: 'Diamond jewellery',
                  val:"DIAMOND_JEWELLERY",
                  check: false,
                  id:9
                },
                {
                  name: 'Turkish jewellery',
                  val:"TURKISH_JEWELLERY",
                  check: false,
                  id:10
                },
                {
                  name: 'Silver jewellery',
                  val:"SILVER_JEWELLERY",
                  check: true,
                  id:11
                },
                {
                  name: 'Plain gold jewellery',
                  val:"PLAIN_GOLD_JEWELLERY",
                  check: false,
                  id:12
                },
                {
                  name: 'Silver articles',
                  val:"SILVER_ARTICLES",
                  check: false,
                  id:13
                },
                {
                  name: 'Gold Mountings',
                  val:"GOLD_MOUNTINGS",
                  check: false,
                  id:14
                },
                {
                  name: 'Gold jewellery',
                  val:"GOLD_JEWELLERY",
                  check: false,
                  id:15
                },
                {
                  name: 'Loose  Diamonds',
                  val:"LOOSE_DIAMONDS",
                  check: true,
                  id:16
                },
                {
                  name: 'Nose Pins',
                  val:"NOSE_PINS",
                  check: false,
                  id:17
                },
                {
                  name: 'Jadau jewellery',
                  val:"JADAU_JEWELLERY",
                  check: false,
                  id:18
                },
              ]

        };
    }

    ModalClose = () => {
        this.setState({ modalVisible: false })
    }

    ModalClose1 = () => {
        this.setState({ modalVisible1: false })
    }

    openModel = (data) => {
        console.log(data)
        if(data&&data.latlng!=null){
         let   obj={
                latitude:data.latlng.lat,
                longitude: data.latlng.lng,
                latitudeDelta: 0.015,
                longitudeDelta: 0.0121,
            }
            this.setState({
                location:obj,
               
            })
        }
        this.setState({
            modalVisible1: true,
            companyName:data.companyName,
            city:data.businessAddress.city
        })
    }

    sendEnquiry=(data)=>{
        if(data!=null){
            let exh={
                id:data.id?data.id:'',
                salesHead:data.salesHead?data.salesHead:'',
                email:data.email?data.email:''
            }
        console.log("data e",exh)
            this.props.navigation.navigate('Enquiry', {
                exh:exh,
                navigate1:3
              });
        }

    }
    bookAppointment=(data)=>{
        console.log("data e",data)
        if(data!=null){
            let exh={
                id:data.id?data.id:'',
                companyName:data.companyName?data.companyName:''
            }
        console.log("data e",exh)
            this.props.navigation.navigate('BookAppointment', {
                exh:exh,
                navigate1:3
              });
        }

    }

    ViewProduct=(data)=>{
        console.log("data e",data)
        if(data!=null){
            let exh={
                id:data.id?data.id:'',
                companyName:data.companyName?data.companyName:'',
                salesHead:data.salesHead?data.salesHead:'',
                email:data.email?data.email:'',
                businessAddress:data.businessAddress?data.businessAddress:'',
                logo:data.logo?data.logo:''
            }
        console.log("data e",exh)
            this.props.navigation.navigate('ProtectedProducts', {
                exh:exh,
                navigate1:1
              });
        }

    }

   
    accessRequest = (i) => {
        if (i.status == null) {
            this.props.client
                .mutate({
                    mutation: createProdAccessRequest,
                    variables: {
                        exhId: i.id,
                        visitorId: this.state.visitorId,
                        name: this.state.name,
                        companyName: this.state.companyName,
                        city: this.state.city,
                        status: "NEW",
                    },
                })
                .then((data) => {
                    console.log("data......", data)
                    let changeStatus = ''
                    changeStatus = this.state.dataSourceNonApp.find((cb) => cb.id === i.id);
                    changeStatus.status = data.data.createProdAccessRequest.status;
                    console.log("c", changeStatus)
                    const arr = Object.assign([], this.state.dataSourceNonApp, changeStatus);
                    this.setState({
                       dataSourceNonApp:arr
                    })
                    Toast.show({
                        text: 'Request For Full Catalogue is sent successfully',
                        type: 'success',
                        position: 'center',
                        duration: 2000,
                        style: { alignSelf: 'center', width: wp('70%'), height: "auto", borderRadius: 10, marginTop: 20 },
                        textStyle: {
                            textAlign: 'center',
                            color: 'black'
                        }
                    });

                })
                .catch((err) => {
                    console.log(err);
                });
        }

        else {
            if (i.status == 'NEW') {
                Toast.show({
                    text: 'Access To Full Catalogue is Sent,Please wait For Approval',
                    type: 'success',
                    position: 'center',
                    duration: 7000,
                    style: { alignSelf: 'center', width: wp('70%'), height: "auto", borderRadius: 10, marginTop: 20 },
                    textStyle: {
                        textAlign: 'center',
                        color: 'black'
                    }
                });

            }
            else {
                if (i.status == 'CANCELLED') {
                    Toast.show({
                        text: 'Access To Full Catalogue Not Available,Please call Exhibitor',
                        type: 'success',
                        position: 'center',
                        duration: 7000,
                        style: { alignSelf: 'center', width: wp('70%'), height: "auto", borderRadius: 10, marginTop: 20 },
                        textStyle: {
                            textAlign: 'center',
                            color: 'black'
                        }
                    });
                }
            }
        }

    };

    createDatasourceProducts=(data)=>{
        this.items = new Set();
        data.length>0 &&data.map((i,j)=>{
            i.productsDealingIn!=null && i.productsDealingIn.length>0 && i.productsDealingIn.map((item)=>{
                this.items.add(item);
            })
        })
        let ProductsArr=[]
        ProductsArr=Array.from(this.items)
        var finalsort= ProductsArr.sort((a, b) => a.toLowerCase() > b.toLowerCase() ? 1 : -1)
        console.log("ProductsArr",ProductsArr)
        let newArr=[]
        finalsort.length>0 && finalsort.map((item)=>{
            this.state.arr.length>0 && this.state.arr.map((i,j)=>{
                if(item==i.val){
                    let  obj={
                        key:i.val,
                        value:i.name
                    }
                    newArr.push(obj)
                }
            })
        })
        console.log("newArr",newArr)

        this.setState({
            ProductsArray:newArr,
            proval:newArr[0].key
        })

    }


    createDatasource=(data)=>{
        let city=[],name=[],Prod=[]
        if(data!=null){
            data.map((i,j)=>{
                let obj1={
                    key:i.businessAddress.city,
                    value:j
                }
                let obj2={
                    key:i.companyName,
                    value:j,
                }
                let obj3={
                    key:i.productsDealingIn[0],
                    value:j
                }
                city.push(obj1)
                name.push(obj2)
                Prod.push(obj3)
            })
            console.log('data source',city,name,Prod)

        }
        var finalsort= city.sort((a, b) => a.key.toLowerCase() > b.key.toLowerCase() ? 1 : -1)
        var finalsort1= name.sort((a, b) => a.key.toLowerCase() > b.key.toLowerCase() ? 1 : -1)
        var finalsort2= Prod.sort((a, b) => a.key.toLowerCase() > b.key.toLowerCase() ? 1 : -1)

        console.log("sort",finalsort1)
  
          
        // Display the list of array objects 
        console.log(finalsort); 
  
        // Declare a new array 
        let newArray = []; 
          
        // Declare an empty object 
        let uniqueObject = {}; 
          
        // Loop for the array elements 
        for (let i in finalsort) { 
  
            // Extract the title 
          let  objTitle = finalsort[i]['key'].toUpperCase(); 
  
            // Use the title as the index 
            uniqueObject[objTitle] = finalsort[i]; 
        } 
          
        // Loop to push unique object into array 
        for (let i in uniqueObject) { 
            newArray.push(uniqueObject[i]); 
        } 
          
        // Display the unique objects 
        console.log(newArray);


// return
        this.setState({
            selcityData:newArray,
            selNameData:finalsort1,
            selProdData:finalsort2,
            cityval:newArray[0].value,
            nameval:finalsort1[0].value,
            proval:finalsort2[0].value
        })
    }


    requestQuery = (exid) => {
        return new Promise((resolve, reject) => {
            this.props.client
            .query({
              query: getProdAccessRequestById,
              fetchPolicy: 'no-cache',
              variables: {
                exhId:exid.id,
                 visitorId: this.state.id,
              },
            })    
            .then(({ data }) => {
          
                if (data && data.getProdAccessRequestById != null) {
                    let obj = {
                        branchesIn: exid.branchesIn,
                        businessAddress: exid.businessAddress,
                        compTel: exid.compTel,
                        companyName: exid.companyName,
                        email: exid.email,
                        enabled: true,
                        id: exid.id,
                        jewExpo: exid.jewExpo,
                        latlng: exid.latlng,
                        logo: exid.logo,
                        marketingCollateral: exid.marketingCollateral,
                        moreProductDetails: exid.moreProductDetails,
                        otherContact: exid.otherContact,
                        ownerName: exid.ownerName,
                        productsDealingIn: exid.productsDealingIn,
                        productsMelting: exid.productsMelting,
                        salesHead: exid.salesHead,
                        slugCompName: exid.slugCompName,
                        target_states: exid.target_states,
                        status: data.getProdAccessRequestById.status
                    }
                    resolve(obj)
                }

                else {
                    let obj1 = {
                        branchesIn: exid.branchesIn,
                        businessAddress: exid.businessAddress,
                        compTel: exid.compTel,
                        companyName: exid.companyName,
                        email: exid.email,
                        enabled: true,
                        id: exid.id,
                        jewExpo: exid.jewExpo,
                        latlng: exid.latlng,
                        logo: exid.logo,
                        marketingCollateral: exid.marketingCollateral,
                        moreProductDetails: exid.moreProductDetails,
                        otherContact: exid.otherContact,
                        ownerName: exid.ownerName,
                        productsDealingIn: exid.productsDealingIn,
                        productsMelting: exid.productsMelting,
                        salesHead: exid.salesHead,
                        slugCompName: exid.slugCompName,
                        target_states: exid.target_states,
                        status: null
                    }
                    resolve(obj1)
                }
            })
            .catch((err) => {
              console.log(`Error : reqQuery : in err: ${JSON.stringify(err)}`);
            });
        });
     
      };

      localData=()=>{
        AsyncStorage.getItem('user')
        .then(data => {
          let data1 = JSON.parse(data);
          if (data1 != null) {
            this.setState({
              id:data1.id,
                visitorId: data1.id,
                name: data1.userName,
                companyName: data1.companyName,
                city: data1.city,
            })
          } 
        })
        .catch(err => console.log('Error in login page', err));
      }

      newFun= async () => {
          let arr=[],brr=[]
        for (let i = 0; i < this.state.data11.length; i++) {
            let reviewData = await this.requestQuery(this.state.data11[i])
            reviewData!=null && reviewData.status=='APPROVED'?arr.push(reviewData):brr.push(reviewData)
        }
        console.log("arr",arr,brr)
        this.setState({
            data:arr,
            data1:arr,
            dataSourceNonApp:brr,
            loading:false
        })
        this.randProc(arr.length);
 this.createDatasource(arr)
this.createDatasourceProducts(arr)

      }

fetchExQuery = () => {
    try {
        this.props.client
            .query({
                query: getJewelExhibitors,
                fetchPolicy: 'no-cache',
                variables: { exhName: 'jewExpo' },
            })
            .then(({ data }) => {
                console.log("data", data)
                this.setState({
                    // loading:false,
                    data11: data.getJewelExhibitors,
                },()=>{this.newFun()})
               
            })
            
            .catch(err => {
                console.log(err);
            });
    } catch (err) {
        console.log('iN catfch', err);
    }
}

    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });
        this.localData()

            this.fetchExQuery()
      

    }

    randProc = (len) => {
        //console.log("Length In Fun::",len)
        var arrayNum = [];
        for (var k = 0; k < len; k++) {
         arrayNum.push(k);
        }
        //console.log("Array",arrayNum);
        var selected = [];
    for (var i = 0; i < len; i++){
        this.rand( arrayNum, selected);
    }
        console.log("RandomData", selected);
        this.setState({
          randomValue: selected
        })
     }
      rand = (arrayNum, selected) => {
      // console.log("inRand:", arrayNum, selected);
        var ran = arrayNum[Math.floor(Math.random() * arrayNum.length)];
        if (selected.indexOf(ran) == -1)
            selected.push(ran);
        else
             this.rand(arrayNum, selected);
    }
    
    
    

    applyFilter=()=>{
        console.log("sam",this.state.proval)
        console.log("sam1",this.state.nameval)
        let city='',arr=[],name=''
            this.state.selcityData.map((i,j)=>{
                if(i.value==this.state.cityval){
                    city=i.key.toLowerCase()
                }
            })
            // this.state.selNameData.map((i,j)=>{
            //     if(i.value==this.state.nameval){
            //         name=i.key
            //     }
            // })
            let provalllll=''
            this.state.data1.map((i,j)=>{
                i.productsDealingIn.map((item)=>{
                    let it=item
                    this.state.ProductsArray.map((ii)=>{
                       if(ii.key==it){
                        provalllll=ii.key
                       }
                        
                    })
                })
            })
            this.state.data1.map((i,j)=>{
                if(i.businessAddress.city.toLowerCase()==city || i.productsDealingIn.includes(provalllll) ){
                        arr.push(i)
                }
            })

            console.log('city',arr) 
            let len =arr.length;
            console.log("Length CMD::",len)
            this.randProc(len);


        this.setState({
            modalVisible:false,
            data:arr,
            applyFilter:true
        })
    }

    clearFilter=()=>{
        this.randProc(this.state.data1.length);
        this.setState({
            data:this.state.data1,
            applyFilter:false,
            modalVisible:false,

        })
    }

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }
    chnageInput=(data)=>{
        console.log("dddd",data)
        let arr=[]
        this.state.data1.map((i,j)=>{
            if(i.companyName.toLowerCase().includes(data.toLowerCase()))
            {
            arr.push(i)
            }
        })
        let len =arr.length;
        console.log("Length CMD::",len)
        this.randProc(len);
        this.setState({
            data:arr
        })
        
   
        if(data=='')
        {
        this.randProc(this.state.data1.length);
            this.setState({
                data:this.state.data1
            })
        }
    }
    searchName=()=>{
        // console.log("dfghjk")
        // let arr=[]
        // this.state.data1.map((i,j)=>{
        //     if(i.companyName.toLowerCase().includes(this.state.companyNameNew.toLowerCase()))
        //     {
        //     arr.push(i)
        //     }
        // })
        // this.setState({
        //     data:arr
        // })
        // console.log("arrrrrrrrr",arr)
    }

    ExhibitorView=(data)=>{
        console.log("img......",data)
        this.props.navigation.navigate('ViewExhibitor', {
            exh:data,
          });
    }

    render() {
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }
        console.log("cli", this.state.data.length)
        return (
        
            <Container style={{ backgroundColor: '#e5e5e5' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 40 }}>
                    <Icon1 onPress={() => { this.props.navigation.navigate('Dashbord') }} style={{ width:wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                    <Text style={{ fontSize: 20, alignSelf: 'center',textAlign:'center',width:wp('80%') }}>Exhibitor List</Text>
                </View>
                <Header searchBar rounded style={{ backgroundColor: '#e5e5e5', elevation: 0 }}>
                    <Item>
                        <Icon onPress={this.searchName} name="ios-search" />
                        <Input placeholder="Search by Exhibitor Name "
                        onChangeText={this.chnageInput.bind(this)} />
                        <Filter onPress={() => { this.setState({ modalVisible: true }) }} style={{ marginRight: 10 }} />
                    </Item>
                </Header>
                { this.state.loading ?
             <Spinner style={{alignSelf:'center',flex:1}} color='blue' />
            :
                <Content >
                    <View style={{ margin: 10 }}>
                            <Text style={{ marginLeft: 10,marginBottom:5 }}>Showing {this.state.data.length} results</Text>
                           </View>

                        <View style={styles.viewCardmap}>
                            {this.state.randomValue.length != 0 && this.state.randomValue.map((k, j) => {
                                var i=this.state.data[k]
                                return (
                                    <Card style={{ width:wp('45%'), height: 250, }}>
                                                {/* {console.log(i&&i.logo!=null?`https://${i.logo.bucket}.s3.${i.logo.region}.amazonaws.com/${i.logo.key}`:'sam')} */}
                                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", margin: 3 }}>
                                            <TouchableWithoutFeedback onPress={()=>{this.ExhibitorView(i)}}>
                                            {i && i.logo != null ?
                                                <Image
                                                style={{
                                                    maxWidth:150,
                                                    maxHeight:100,
                                                    width:150,
                                                    height:95,
                                                    resizeMode: 'stretch',
                                                  }}
                                                    source={{
                                                        uri: `https://${i.logo.bucket}.s3.${i.logo.region}.amazonaws.com/${i.logo.key}`,
                                                    }}
                                                /> :
                                                <Image
                                                    style={{ height: 90, width: 100 }}
                                                    source={require('../Public/img/blank.png')}
                                                />
                                            }
                                            </TouchableWithoutFeedback>
                                            <Text style={{ fontWeight: 'bold', fontSize: 13, marginTop: 4, color: '#403e3e', textAlign: 'center' }}>{i.companyName}</Text>
                                            {/* <Text style={{ color: '#8c3d2e', fontSize: 15 }}>Jewelry Designer</Text> */}
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', }}>
                                                <Location onPress={()=>this.openModel(i)} style={{}} width={20} height={20} />
                                                <Text style={{ fontSize: 14, marginLeft: 2, color: '#636161' }}>{i.businessAddress.city}</Text>
                                            </View>
                                            <Text></Text>
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', position: 'absolute', bottom: 0, marginBottom: 10 }}>
                                                <Callender onPress={()=>this.bookAppointment(i)} style={{ marginRight: 30 }} width={20} height={25} />
                                                <Message onPress={()=>{this.sendEnquiry(i)}} width={20} height={25} />
                                                <Product onPress={()=>{this.ViewProduct(i)}}  style={{ marginLeft: 25 }} width={27} height={27} />
                                            </View>
                                        </View>

                                    </Card>
                                )
                            })}

                        </View>



                                {this.state.dataSourceNonApp.length>0 && this.state.dataSourceNonApp.map((i,j)=>{
                                    return(
                                        <Card style={{alignSelf:'center',height:'auto',width:wp('95%'),marginTop:10}}>
                                            <View style={{flexDirection:'row',justifyContent:'space-between',margin:10}}>
                                                <View style={{width:wp('50%')}}>
                                                {i && i.logo != null ?
                                                <Image
                                                style={{
                                                    maxWidth:100,
                                                    maxHeight:100,
                                                    width:100,
                                                    height:95,
                                                    resizeMode: 'center',
                                                    alignSelf:'center'
                                                  }}
                                                    source={{
                                                        uri: `https://${i.logo.bucket}.s3.${i.logo.region}.amazonaws.com/${i.logo.key}`,
                                                    }}
                                                /> :
                                                <Image
                                                    style={{ height: 90, width: 100,resizeMode: 'center',
                                                    alignSelf:'center' }}
                                                    source={require('../Public/img/blank.png')}
                                                />
                                            }
                                            <Text style={{ fontWeight: 'bold', fontSize: 13, marginTop: 4, color: '#403e3e', textAlign: 'center' }}>{i.companyName}</Text>
                                                   
                                                </View>
                                                <Button style={{ width: wp('38%'), backgroundColor: '#C4941C', borderRadius: 10,alignSelf:'center' }}
                                                onPress={()=>{this.accessRequest(i)}}
                                                    full>
                                                    <Text style={{textAlign:'center'}}>Request Access</Text>
                                                </Button>
                                            </View>
                                        </Card>
                                    )
                                })}
                    {/* </View> */}

                </Content>
                }
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible}
                  
                >
                    <Container>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 50,backgroundColor: '#eff1f5e0' }}>
          <Icon1 onPress={this.ModalClose} style={{ width:wp('20%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
          <Text style={{ fontSize: 24,textAlign:'center',width:wp('65%'),alignSelf:'center', }}>Filter</Text>
        </View>
                        <Content>
                            <View style={{ margin: 10 }}>
                                {/* <Item>
                                    <Left>

                                        <Text style={{ marginTop: -10,width:wp('15%') }}>Name</Text>
                                    </Left>
                                    <Body>
                                        <Picker
                                            selectedValue={this.state.nameval}
                                            style={{ height: 50, width:wp('60%'), marginTop: -10 }}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ nameval: itemValue })}
                                        >
                                            {this.state.selNameData.length!=0 &&this.state.selNameData.map((i,j)=>{
                                                return(
                                                    <Picker.Item label={i.key} value={i.value} />
                                                )
                                            })}
                                        </Picker>
                                    </Body>
                                </Item>
                             */}
                                <Item>
                                    <Left>

                                        <Text style={{ marginTop: -10,width:wp('15%') }}>City</Text>
                                    </Left>
                                    <Body>
                                        <Picker
                                            selectedValue={this.state.cityval}
                                            style={{ height: 50, width:wp('60%'), marginTop: -10 }}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ cityval: itemValue })}
                                        >
                                            {this.state.selcityData.length!=0 &&this.state.selcityData.map((i,j)=>{
                                                return(
                                                    <Picker.Item label={i.key} value={i.value} />
                                                )
                                            })}
                                        </Picker>
                                    </Body>
                                </Item>
                                <Item>
                                    <Left>

                                        <Text style={{ marginTop: -10 ,width:wp('20%')}}>Product</Text>
                                    </Left>
                                    <Body>
                                        <Picker
                                            selectedValue={this.state.proval}
                                            style={{ height: 50, width:wp('60%'), marginTop: -10 }}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ proval: itemValue })}
                                        >
                                               {this.state.ProductsArray.length!=0 &&this.state.ProductsArray.map((i,j)=>{
                                                return(
                                                    <Picker.Item label={i.value} value={i.key} />
                                                )
                                            })}
                                        </Picker>
                                    </Body>
                                </Item>

                               <Button style={{ alignSelf:'center', backgroundColor: '#B88A1C',marginTop:20,width:wp('91%') }} 
                               onPress={this.applyFilter} full>
                                   <Text>Apply Filter</Text>
                                   </Button>
                       {this.state.applyFilter &&

                                <Button style={{ alignSelf:'center', backgroundColor: '#B88A1C',marginTop:20,width:wp('91%') }} 
                                onPress={this.clearFilter} full>
                                    <Text>Clear Filter</Text>
                                    </Button>
                       }

                            </View>
                        </Content>
                    </Container>
                </Modal>

                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.modalVisible1}
                   
                >
                    <Container>
                        <Header style={{ backgroundColor: '#eff1f5e0', elevation: 0 }}>
                            <Left>
                                <Icon1 onPress={this.ModalClose1} name="arrowleft" size={25} />
                            </Left>
                            <Body>
                                <Text style={{ fontSize: 24 }}>Location</Text>
                            </Body>
                        </Header>
                        <Content>
                            <View style={{ margin: 10 }}>
                                <MapView data={this.state.location} companyName={this.state.companyName} city={this.state.city} />
                            </View>
                        </Content>
                    </Container>
                </Modal>
{!this.state.loading &&
                <Footer >
                    <FooterTab style={{ backgroundColor: '#2E2D33' }}>

                        <Button vertical >
                            <Home height={25} width={25} />
                            <Text style={{ fontSize: 8 }}>Home</Text>
                        </Button>

                        <Button vertical>
                            <Enquiry height={25} width={25}  />
                            <Text style={{ fontSize: 8 }}>My List</Text>
                        </Button>

                        <Button vertical  onPress={() => { this.props.navigation.navigate('Notification') }}>
                        <Notification height={25} width={25} />
                    <Text style={{ fontSize: 8 }}>Notifications</Text>
                    </Button>

                    <Button vertical  onPress={() => { this.props.navigation.navigate('Profile') }}>
                        <Profile  height={25} width={25}/>
                    <Text style={{ fontSize: 8 }}>My Profile</Text>
                    </Button>
                    </FooterTab>

                </Footer>
}
            </Container>
            

        
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {

    },
    viewCardmap: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        display: 'flex',
        flex: 1,
        width:wp('94%'),
        alignSelf:'center'
    },


});


export default withApollo(Manufacturers)
