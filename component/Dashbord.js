import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    TouchableWithoutFeedback,
    AsyncStorage
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import {withApollo, compose, graphql} from 'react-apollo';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Icon, Text, View, Card, Left, Right, Thumbnail, Body
} from 'native-base';
import Icon1 from 'react-native-vector-icons/AntDesign'
import Svg1 from '../Public/icon/Search Product_icon.svg';
import Svg2 from '../Public/icon/manufacturers_icon.svg';
import Svg3 from '../Public/icon/Book Appointment_icon.svg';
import Svg4 from '../Public/icon/Post Demand_icon.svg';
import Menu from '../Public/icon/menu.svg'
import Search from '../Public/icon/Search.svg'
import Home from '../Public/icon/Home.svg'
import Enquiry from '../Public/icon/enquiry.svg'
import Notification from '../Public/icon/notification.svg'
import Profile from '../Public/icon/profile.svg'



let arr = [
    {
        name: 'Search Products',
        color: '#B19FB2',
        svg: 'Svg1',
        route:'SearchProduct'
    },
    {
        name: 'Search Exhibitors',
        color: '#B3C3B8',
        svg: 'Svg2',
        route:'Manufacturers'

    },
    {
        name: 'Manage Appointments',
        color: '#B09D98',
        svg: 'Svg3',
        route:'Appointment'

    },
    {
        name: 'Post Demand',
        color: '#CA9C80',
        svg: 'Svg4',
        route:'PostDemand'

    }
]
 class Dashbord extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected:true
        };
    }

    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
          });
      }
    
      componentWillMount() {
        NetInfo.fetch().then(isConnected => {
          this.setState({isConnected});
        });
    }    

        logout = async () => {
            console.log('IN logouts');
            await AsyncStorage.removeItem('user');
            this.props.navigation.navigate('Login');
          };


    render() {
        console.log("props",this.props)
         if (!this.state.isConnected) {
             return <Text>></Text>;
         }
        return (
            <Container>
            <Header style={{ backgroundColor: '#eff1f5e0', height: 70 }}>
                <Left>
                    <Menu />
                </Left>
                <Body>
                    <Thumbnail style={{ width: 180,height:60,alignSelf:'stretch', marginLeft:30 }} square source={require('../Public/img/image.png')} />
                </Body>
                <Right>
                    <Icon1 onPress={this.logout} name="logout" size={25}/>
                </Right>

            </Header>

            <Content style={{ }}>
                    <Image
                        style={styles.header_img}
                        source={require('../Public/img/bannerr.jpeg')}
                    />
                    <View style={{margin:10}}>
                        <View style={styles.viewCardmap}>
                            {arr.map((i, j) => {
                                return (
                                    <TouchableWithoutFeedback onPress={() => { this.props.navigation.navigate(`${i.route}`) }}  >
                                    <Card style={{ height:hp('23.5%'), width:wp('45%'), backgroundColor: i.color, }}>
                                        <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                                            {i.svg == 'Svg1' ? <Svg1 /> : i.svg == 'Svg2' ? <Svg2 /> : i.svg == 'Svg3' ? <Svg3 /> : <Svg4 />}
                                            {/* <Text></Text> */}
                                            <Text style={{ color: 'white', fontWeight: 'bold',textAlign:'center',marginTop:7 }}>{i.name}</Text>
                                        </View>

                                    </Card>
                                    </TouchableWithoutFeedback>

                                )
                            })}

                        </View>

                    </View>
            </Content>
            <Footer style={{ borderTopLeftRadius: 60 }}>
                <FooterTab style={{ backgroundColor: '#2E2D33' }}>

                    <Button vertical >
                        <Home height={25} width={25} />
                        <Text style={{ fontSize: 8 }}>Home</Text>
                    </Button>

                    <Button vertical onPress={() => { this.props.navigation.navigate('Approvedexhibitors') }}>
                        <Enquiry height={25} width={25} />
                        <Text style={{ fontSize: 8 }}>My List</Text>
                    </Button>
                    <Button vertical  onPress={() => { this.props.navigation.navigate('Notification') }}>
                        <Notification height={25} width={25} />
                    <Text style={{ fontSize: 8 }}>Notifications</Text>
                    </Button>

                    <Button vertical  onPress={() => { this.props.navigation.navigate('Profile') }}>
                        <Profile height={25} width={25} />
                    <Text style={{ fontSize: 8 }}>My Profile</Text>
                    </Button>
                </FooterTab>

            </Footer>

        </Container>
  
      );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        alignSelf: 'center',
        height:200,
        width:wp('93.2%'),
        // resizeMode: 'stretch',
        marginTop:15,
    },
    viewCardmap: {
        textAlign:'center',
        flexDirection: 'row',
        justifyContent: 'space-between',
        // alignContent: 'center',
        // alignItems: 'center',
        flexWrap: 'wrap',
        // display: 'flex',
    },


});

export default withApollo(Dashbord)