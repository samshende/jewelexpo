import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Picker,
    Modal
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import Icon1 from 'react-native-vector-icons/AntDesign'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import getAllJewProducts from '../GraphQl/getAllJewProducts'
import getJewelExhibitorByMobForApp from '../GraphQl/getExhibitorByMob'
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Item, Input, Icon,Spinner
} from 'native-base';
import ImageZoom from 'react-native-image-pan-zoom';
import Home from '../Public/icon/Home.svg'
import Enquiry from '../Public/icon/enquiry.svg'
import Notification from '../Public/icon/notification.svg'
import Profile from '../Public/icon/profile.svg'
import Icon2 from '../Public/icon/like_black.svg'
import Message from '../Public/icon/message.svg'
import Filter from '../Public/icon/Filter.svg'

let arr = [
    {
        name: 'Wedding Diamond Rings',
        img: require('../Public/img/Product_Search_img_1.png'),
        price: 20.02
    },
    {
        name: 'Couple Silver Ring',
        img: require('../Public/img/Product_Search_img_2.png'),
        price: 100.24
    },
    {
        name: 'Silver Diamond Rings',
        img: require('../Public/img/Product_Search_img_3.png'),
        price: 50.02
    },
    {
        name: 'Chocolate Diamond Rings',
        img: require('../Public/img/Product_Search_img_4.png'),
        price: 150.24
    },
    {
        name: 'Rose Gold Diamond Rings',
        img: require('../Public/img/Product_Search_img_5.png'),
        price: 500.02
    },
    {
        name: 'Platinum Emerald Rings',
        img: require('../Public/img/Product_Search_img_6.png'),
        price: 420.24
    }
]
class SearchProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedValue: 'product',
            modalVisible: false,
            isConnected: true,
            data: [],
            data1: [],
            categoryval: '',
            selcategoryData: [],
            applyFilter: false,
            loading:true

        };
    }
    ModalClose = () => {
        this.setState({ modalVisible: false })
    }


    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });
        try {
            this.props.client
                .query({
                    query: getAllJewProducts,
                    fetchPolicy: 'no-cache',
                    variables: { limit: 50, nextToken: '' },
                })
                .then(({ data }) => {
                    console.log("data....", data)
                    this.setState({
                        data: data.getAllJewProducts.items,
                        loading: false,
                        data1: data.getAllJewProducts.items,
                    }, () => { this.createDatasource(data.getAllJewProducts.items) })

                })

                .catch(err => {
                    console.log(err);
                });
        } catch (err) {
            console.log('iN catfch', err);
        }

    }

    chnageInput = (data) => {
        console.log("dddd", data)
        let arr = []
        this.state.data1.map((i, j) => {
            if (i.category.toLowerCase().includes(data.toLowerCase())) {
                arr.push(i)
            }
        })
        this.setState({
            data: arr
        })


        if (data == '') {
            this.setState({
                data: this.state.data1
            })
        }
    }

    createDatasource = (data) => {
        let category = []
        if (data != null) {
            data.map((i, j) => {
                let obj1 = {
                    key: i.category,
                    value: j
                }
                category.push(obj1)
            })
        }
        var finalsort2 = category.sort((a, b) => a.key.toLowerCase() > b.key.toLowerCase() ? 1 : -1)
        console.log('data source', finalsort2)

        this.setState({
            selcategoryData: finalsort2,
            categoryval: finalsort2[0].value
        })
    }

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }

    applyFilter = () => {
        console.log("sam1", this.state.categoryval)
        let category1 = '', arr = [];
        this.state.selcategoryData.map((i, j) => {
            if (i.value == this.state.categoryval) {
                category1 = i.key
            }
        })

        console.log("cat.......", category1)
        this.state.data1.map((i, j) => {
            if (i.category == category1) {
                arr.push(i)
            }
        })
        console.log('city', arr)



        this.setState({
            modalVisible: false,
            data: arr,
            applyFilter: true
        })
    }

    clearFilter = () => {
        this.setState({
            data: this.state.data1,
            applyFilter: false,
            modalVisible: false,

        })
    }

    EnquiryFun = (data) => {
        let arr=[]
        if(data.image!=null)
        arr.push(data)

        console.log(arr)

        // return
        let data11=data.exhId
        try {
            this.props.client
                .query({
                    query: getJewelExhibitorByMobForApp,
                    fetchPolicy: 'no-cache',
                    variables: { mob: data.exhId },
                })
                .then(({ data }) => {
                    if (data.getJewelExhibitorByMobForApp != null) {
                        console.log(data)
                        let exh={
                            id:data11,
                            salesHead:data.getJewelExhibitorByMobForApp.salesHead?data.getJewelExhibitorByMobForApp.salesHead:'',
                            email:data.getJewelExhibitorByMobForApp.email?data.getJewelExhibitorByMobForApp.email:''
                        }
                    console.log("data e",exh)
                        this.props.navigation.navigate('EnquiryImage', {
                            exh:exh,
                            navigate1:2,
                            image:arr

                          });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        } catch (err) {
            console.log('iN catfch', err);
        }

    }

    exhibitorFun=(data)=>{
        console.log(data.exhId)
        let data11=data.exhId
        try {
            this.props.client
                .query({
                    query: getJewelExhibitorByMobForApp,
                    fetchPolicy: 'no-cache',
                    variables: { mob: data.exhId },
                })
                .then(({ data }) => {
                    if (data.getJewelExhibitorByMobForApp != null) {
                        console.log(data.getJewelExhibitorByMobForApp)
                        this.props.navigation.navigate('ViewExhibitor', {
                            exh:data.getJewelExhibitorByMobForApp,
                            navigate1:2
                          });
                    }
                })
                .catch(err => {
                    console.log(err);
                });
        } catch (err) {
            console.log('iN catfch', err);
        }

    }

    render() {
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }
        console.log("this.state", this.state.data)

        return (
            <Container style={{ backgroundColor: '#e5e5e5' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 50 }}>
                    <Icon1 onPress={() => { this.props.navigation.navigate('Dashbord') }} style={{ width:wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                    <Text style={{ fontSize: 22, alignSelf: 'center',width:wp('80%'),textAlign:'center'  }}>Search Results</Text>
                </View>
                <Header searchBar rounded style={{ backgroundColor: '#e5e5e5', elevation: 0 }}>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Search Product By Category"
                            onChangeText={this.chnageInput.bind(this)} />
                        {/* <Filter onPress={() => { this.setState({ modalVisible: true }) }} style={{ marginRight: 10 }} /> */}
                    </Item>
                </Header>
                { this.state.loading ?
             <Spinner style={{alignSelf:'center',flex:1}} color='blue' />
            :
                <Content >
                    <View style={{ margin: 10 }}>
                        <Text style={{ marginLeft: 10, marginBottom: 5 }}>Showing {this.state.data.length} results</Text>
                    </View>
                    <View style={styles.viewCardmap}>
                        {this.state.data.length > 0 && this.state.data.map((i, j) => {
                            return (
                                <Card style={{ width: wp('45%'), height: 200 }}>
                                    <View style={{ flex: 1, justifyContent: "center", alignItems: "center", }}>
                                        {/* <Card style={{ width: 20, height: 20, position: 'absolute', right: 0, top: 0, marginRight: 5 }}>
                                            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                                                <Icon2 />
                                            </View>
                                        </Card> */}
                                        {i && i.image != null ?
                                         <ImageZoom cropWidth={100}
                                         cropHeight={100}
                                         imageWidth={100}
                                         imageHeight={100}>
                                            <Image
                                                style={styles.header_img}
                                                source={{
                                                    uri: `https://${i.image.bucket}.s3.${i.image.region}.amazonaws.com/${i.image.key}`,
                                                }}
                                            />
                                            </ImageZoom>
                                            :
                                            <Image
                                                style={{ height: 90, width: 90 }}
                                                source={require('../Public/img/Product_Search_img_2.png')}
                                            />
                                        }
                                        <Text style={{ marginLeft: 5, marginTop: 5, textAlign: 'center' ,fontSize:13,marginRight:5}}>{i.category ? i.category : ''}</Text>
                                        <Text></Text>
                                        <View style={{position: 'absolute', marginBottom: 10, bottom: 0,justifyContent:'space-between',flexDirection:'row' ,alignSelf:'center',width:wp('30%')}}>
                                            <Message onPress={() => { this.EnquiryFun(i) }} style={{}} width={20} height={25} />
                                                <Profile onPress={()=>{this.exhibitorFun(i)}} style={{}} width={25} height={25} />
                                                </View>

                                    </View>

                                </Card>
                            )
                        })}

                    </View>



                </Content>
                }
                <Modal
                    transparent={true}
                    visible={this.state.modalVisible}
                >
                    <Container>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 50, backgroundColor: '#eff1f5e0' }}>
                            <Icon1 onPress={this.ModalClose} style={{ width: wp('20%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                            <Text style={{ fontSize: 24, textAlign: 'center', width: wp('65%'), alignSelf: 'center', }}>Filter</Text>
                        </View>
                        <Content>
                            <View style={{ margin: 10 }}>
                                <Item>
                                    <Left>

                                        <Text style={{ marginTop: -10, width: wp('20%') }}>Category</Text>
                                    </Left>
                                    <Body>
                                        <Picker
                                            selectedValue={this.state.categoryval}
                                            style={{ height: 50, width: wp('60%'), marginTop: -10 }}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ categoryval: itemValue })}
                                        >
                                            {this.state.selcategoryData.length != 0 && this.state.selcategoryData.map((i, j) => {
                                                return (
                                                    <Picker.Item label={i.key} value={i.value} />
                                                )
                                            })}
                                        </Picker>
                                    </Body>
                                </Item>
                                <Item>
                                    <Left>

                                        <Text style={{ marginTop: -10, width: wp('20%') }}>Type</Text>
                                    </Left>
                                    <Body>
                                        <Picker
                                            selectedValue={this.state.categoryval}
                                            style={{ height: 50, width: wp('60%'), marginTop: -10 }}
                                            onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue: itemValue })}
                                        >
                                         <Picker.Item label="VIEW_ALL" value="java" />
                                        </Picker>
                                    </Body>
                                </Item>
                               
                              </View>
                            <Button style={{ alignSelf: 'center', backgroundColor: '#B88A1C', marginTop: 20, width: wp('91%') }}
                                onPress={this.applyFilter} full>
                                <Text>Apply Filter</Text>
                            </Button>
                            {this.state.applyFilter &&

                                <Button style={{ alignSelf: 'center', backgroundColor: '#B88A1C', marginTop: 20, width: wp('91%') }}
                                    onPress={this.clearFilter} full>
                                    <Text>Clear Filter</Text>
                                </Button>
                            }
                        </Content>
                    </Container>


                </Modal>
{!this.state.loading &&

                <Footer >
                    <FooterTab style={{ backgroundColor: '#2E2D33' }}>

                        <Button vertical >
                            <Home  height={25} width={25}/>
                        <Text style={{ fontSize: 8 }}>Home</Text>
                        </Button>

                        <Button vertical onPress={() => { this.props.navigation.navigate('Approvedexhibitors') }}>
                        <Enquiry height={25} width={25} />
                        <Text style={{ fontSize: 8 }}>My List</Text>
                    </Button>

                        <Button vertical  onPress={() => { this.props.navigation.navigate('Notification') }}>
                        <Notification height={25} width={25} />
                    <Text style={{ fontSize: 8 }}>Notification</Text>
                    </Button>

                    <Button vertical  onPress={() => { this.props.navigation.navigate('Profile') }}>
                        <Profile height={25} width={25} />
                    <Text style={{ fontSize: 8 }}>Profile</Text>
                    </Button>
                    </FooterTab>

                </Footer>
}

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        height: 100, width: 100
    },
    viewCardmap: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignContent: 'center',
        alignItems: 'center',
        flexWrap: 'wrap',
        display: 'flex',
        flex: 1,
        width: wp('94%'),
        alignSelf: 'center'
    },


});
export default withApollo(SearchProduct)