import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  Image,
  Picker,
  Dimensions,
  TouchableWithoutFeedback,
  PermissionsAndroid,
  AsyncStorage
} from 'react-native'
import Icon1 from 'react-native-vector-icons/AntDesign'
import NetInfo from "@react-native-community/netinfo";
import ImagePicker from 'react-native-image-picker';
import createVisitor from '../GraphQl/createBasicVisitor'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import { withApollo, compose, graphql } from 'react-apollo';
import {
  Container, Header, Content, Footer, FooterTab, Button,Toast, Text, View,Spinner, Card, Left, Right, Thumbnail, Body, Item, Input, Icon, Form, Label, CheckBox, Grid, Col, Radio
} from 'native-base';
import configVariables from '../config/AppSync'

import Home from '../Public/icon/Home.svg'
import Enquiry from '../Public/icon/enquiry.svg'
import Notification from '../Public/icon/notification.svg'
import Profile from '../Public/icon/profile.svg'

const requestCameraPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "Cool Photo App Camera Permission",
        message:
          "Cool Photo App needs access to your camera " +
          "so you can take awesome pictures.",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.log(err);
  }
};


const stateOptions = {
  // "ALL_STATES": "All States",
  JAMMU_KASHMIR: "Jammu & Kashmir",
  HIMACHAL_PRADESH: "Himachal Pradesh",
  PUNJAB: "Punjab",
  DELHI: "Delhi",
  RAJASTHAN: "Rajasthan",
  UTTARAKHAND: "Uttarakhand",
  UTTAR_PRADESH: "Uttar Pradesh",
  WEST_BENGAL: "West Bengal",
  BIHAR: "Bihar",
  JHARKHAND: "Jharkhand",
  ODISHA: "Odisha",
  ASSAM: "Assam",
  ARUNACHAL_PRADESH: "Arunachal Pradesh",
  MEGHALAYA: "Meghalaya",
  TRIPURA: "Tripura",
  MIZORAM: "Mizoram",
  MANIPUR: "Manipur",
  NAGALAND: "Nagaland",
  MAHARASHTRA: "Maharashtra",
  GUJARAT: "Gujarat",
  GOA: "Goa",
  CHHATTISGARH: "Chhattisgarh",
  MADHYA_PRADESH: "Madhya Pradesh",
  ANDHRA_PRADESH: "Andhra Pradesh",
  HARYANA: "Haryana",
  KERALA: "Kerala",
  SIKKIM: "Sikkim",
  TAMIL_NADU: "Tamil Nadu",
  TELANGANA: "Telangana",
  KARNATAKA: "Karnataka",
};

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: 'MAHARASHTRA',
      isConnected: true,
      gender: 'MALE',
      fName: '',
      lName: '',
      companyName: '',
      designation: '',
      add1: '',
      add2: '',
      city: '',
      pin: '',
      ccode: '',
      email: '',
      gst: '',
      Telephone: '',
      productsInterestedIn: [],
      selectedFruits: [],
      profile1: '',
      profilePhoto: '',
      proFileName: '',

      profile: '',
      profilePhoto1: '',
      proFileName1: '',
      loading:false,
      arr :[
        {
         name:'Gold Chain',
         val:'GOLD_CHAINS',
         check:false,
         id:1
        },
        {
         name:'Mangalsutras',
         val:'MANGALSUTRAS',
         check:false,
         id:2
        },
        {
         name:'Bangles',
         val:'BANGLES',
         check:false,
         id:3
        },
        {
         name:'Bracelets',
         val:'BRACELETS',
         check:false,
         id:4
        },
        {
         name:'Casting jewellery',
         val:'CASTING_JEWELLERY',
         check:false,
         id:5
        },
        {
         name:'CZ Diamond jewellery',
         val:'CZ_DIAMOND_JEWELLERY',
         check:false,
         id:6
        },
        {
         name:'Antique jewellery',
         val:'ANTIQUE_JEWELLERY',
         check:false,
         id:7
        },
        {
         name:'Temple jewellery',
         val:'TEMPLE_JEWELLERY',
         check:false,
         id:8
        },
        {
         name:'Kolkata jewellery',
         val:'KOLKATA_JEWELLERY',
         check:false,
         id:9
        },
        {
         name:'Imported italian jewellery',
         val:'IMPORTED_ITALIAN_JEWELLERY',
         check:false,
         id:10
        },
        {
         name:'Lightweight jewellery',
         val:'LIGHTWEIGHT_JEWELLERY',
         check:false,
         id:11
        },
        {
         name:'Designer antique jewellery',
         val:'DESIGNER_ANTIQUE_JEWELLERY',
         check:false,
         id:12
        },
        {
         name:'Plain gold jewellery',
         val:'PLAIN_GOLD_JEWELLERY',
         check:false,
         id:13
        },
        {
         name:'real diamond jewellery',
         val:'REAL_DIAMOND_JEWELLERY',
         check:false,
         id:14
        },
        {
         name:'Platinum jewellery',
         val:'PLATINUM_JEWELLERY',
         check:false,
         id:15
        },
        {
         name:'925 Silver jewellery',
         val:'SILVER_JEWELLERY_925',
         check:false,
         id:16
        },
        {
         name:'Silver articles',
         val:'SILVER_ARTICLES',
         check:false,
         id:17
        },
        {
         name:'Loose diamonds',
         val:'LOOSE_DIAMONDS',
         check:false,
         id:18
        },
        {
         name:'Diamond jewellery',
         val:'DIAMOND_JEWELLERY',
         check:false,
         id:19
        },
        {
         name:'Gold jewellery',
         val:'GOLD_JEWELLERY',
         check:false,
         id:20
        },
        {
         name:'Jadau jewellery',
         val:'JADAU_JEWELLERY',
         check:false,
         id:21
        },  
        {
         name:'Silver jewellery',
         val:'SILVER_JEWELLERY',
         check:false,
         id:22
        },
        {
         name:'Machinery allied section',
         val:'MACHINERY_ALLIED_SECTION',
         check:false,
         id:23
        },
        {
         name:'Rose Gold Jewellery',
         val:'ROSE_GOLD_JEWELLERY',
         check:false,
         id:24
        },
        {
         name:'Rajkot Jewellery',
         val:'RAJKOT_JEWELLERY',
         check:false,
         id:25
        },
        {
         name:'Coimbatore Jewellery',
         val:'COIMBATORE_JEWELLERY',
         check:false,
         id:26
        },
        {
         name:'Kids Jewellery',
         val:'EMERALD_JEWELLERY',
         check:false,
         id:27
        },
        {
         name:'Emerald Jewellery',
         val:'EMERALD_JEWELLERY',
         check:false,
         id:28
        },
        {
         name:'CNC Mala',
         val:'CNC_MALA',
         check:false,
         id:29
        },
        {
         name:'Gemstones',
         val:'GEMSTONES',
         check:false,
         id:30
        },
        {
         name:'Emerald',
         val:'EMERALD',
         check:false,
         id:31
        },
        {
         name:'Gold Mountings',
         val:'GOLD_MOUNTINGS',
         check:false,
         id:32
        },
        {
         name:'Turkish Jewellery',
         val:'TURKISH_JEWELLERY',
         check:false,
         id:33
        },
        {
         name:'Nose Pins',
         val:'NOSE_PINS',
         check:false,
         id:34
        },
     ]
    };
  }


  getBlob = response => {
    console.log('in getBlob ::', response);
    const xhr = new XMLHttpRequest();
    xhr.open('GET', response.uri, true);
    xhr.responseType = 'blob';
    xhr.onload = () => {
      const reader = new FileReader();
      reader.onloadend = () => {
        let myBase64 = reader.result;
        var buf = new Buffer(
          myBase64.replace(/^data:image\/\w+;base64,/, ''),
          'base64',
        );

        this.setState({
          profilePhoto: buf,
          proFileName: response.fileName,
        });
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.send();
  };

  delete = () => {
    this.setState({
      proFileName: '',
      profile1: '',
      profilePhoto: '',
    });
  };
  selectPhotoTapped() {
    requestCameraPermission()
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      title: 'Select Photo',
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response showImagePicker = ', response);
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri, fileName: response.fileName };
        console.log("source",source)
        console.log("source",`data:image/png;base64,${response.data}`)
        this.getBlob(source);
        this.setState({
          profile1: `data:image/png;base64,${response.data}`,
        });
      }
    });
  }

  getBlob1 = response => {
    console.log('in getBlob ::', response);
    const xhr = new XMLHttpRequest();
    xhr.open('GET', response.uri, true);
    xhr.responseType = 'blob';
    xhr.onload = () => {
      const reader = new FileReader();
      reader.onloadend = () => {
        let myBase64 = reader.result;
        var buf = new Buffer(
          myBase64.replace(/^data:image\/\w+;base64,/, ''),
          'base64',
        );

        this.setState({
          profilePhoto1: buf,
          proFileName1: response.fileName,
        });
      };
      reader.readAsDataURL(xhr.response);
    };
    xhr.send();
  };

  delete1 = () => {
    this.setState({
      proFileName1: '',
      profile: '',
      profilePhoto1: '',
    });
  };
  selectPhotoTapped1() {
    requestCameraPermission()
    const options = {
      quality: 1.0,
      maxWidth: 500,
      maxHeight: 500,
      title: 'Select Photo',
      storageOptions: {
        skipBackup: true,
      },
    };

    ImagePicker.showImagePicker(options, response => {
      console.log('Response showImagePicker = ', response);
      if (response.didCancel) {
        console.log('User cancelled photo picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
      } else {
        let source = { uri: response.uri, fileName: response.fileName };
        console.log("source",source)
        console.log("source",`data:image/png;base64,${response.data}`)
        this.getBlob1(source);
        this.setState({
          profile: `data:image/png;base64,${response.data}`,
        });
      }
    });
  }


  componentDidMount() {
    NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
    });
  }

  componentWillMount() {
    NetInfo.fetch().then(isConnected => {
      this.setState({ isConnected });
    });
  }

  toggleCheckbox=(id)=> {
    console.log("id",id)
    let changedCheckbox=''
    changedCheckbox= this.state.arr.find((cb) => cb.id === id);
    changedCheckbox.check = !changedCheckbox.check;
    console.log("c",changedCheckbox)
    const arr = Object.assign([], this.state.arr,changedCheckbox);
    this.setState({ arr });
  }

  
  enterLoading=()=>{
    this.setState({
      loading:true
    })
  }

  handleSubmit = () => {
    this.enterLoading()
    if (this.state.companyName == '' || this.state.fName == '' || this.state.add1 == '' || this.state.pin == '' || this.state.designation == '' || this.state.city == '' ||
      this.state.lName == '' || this.state.add2 == '' || this.state.gender == '' || this.state.ccode == '' || this.state.email == '' || this.state.gst == '' || this.state.Telephone == '' || this.state.selectedValue == '' || this.state.profilePhoto1 == '' || this.state.profilePhoto == '') {
      this.setState({
        error: true,
        loading:false
      })
      return
    }
    else {

      let productsInterestedIn=[]
this.state.arr.map((i)=>{
  if(i.check==true){
    productsInterestedIn.push(i.val)
  }
})
console.log('piiiii.....',productsInterestedIn)

      let   testphoto=this.state.profilePhoto != ''
      ?
     {
          bucket: configVariables.s3Bucket,
          region: configVariables.region,
          localUri: this.state.profilePhoto,
          key: `JewelExpo/Images/${this.state.proFileName}`,
          mimeType: 'image/jpeg',
        }
        :''
      console.log('1',testphoto)

      let testphoto1 =this.state.profilePhoto1 != '' ?
         {
             bucket: configVariables.s3Bucket,
             region: configVariables.region,
             localUri: this.state.profilePhoto1,
             key: `JewelExpo/Images/${this.state.proFileName1}`,
             mimeType: 'image/jpeg',
           }:''
         console.log('2',testphoto1)
      // return
      let basicVisitor = {
        fName: this.state.fName ? this.state.fName : undefined,
        lName: this.state.lName ? this.state.lName : undefined,
        companyName: this.state.companyName ? this.state.companyName : undefined,
        address: {
          addressLineOne: this.state.add1 ? this.state.add1 : undefined,
          addressLineTwo: this.state.add2 ? this.state.add2 : undefined,
          city: this.state.city ? this.state.city : undefined,
          state: this.state.selectedValue,
          zip: this.state.pin ? this.state.pin : undefined,
          country: "IND",
        },
        designation: this.state.designation ? this.state.designation : undefined,
        telephone: this.state.Telephone ? this.state.Telephone : undefined,
        email: this.state.email ? this.state.email : undefined,
        gstNo: this.state.gst ? this.state.gst : undefined,
        countryCode: this.state.ccode ? this.state.ccode : undefined,
        gender: this.state.gender ? this.state.gender : undefined,
        visitorPhoto: testphoto1,
        docPhoto: testphoto,
      };

      let jewExpo = {
        exhName: "jewExpo",
        productsInterestedIn:productsInterestedIn.length>0?productsInterestedIn:undefined,
        status: "NEW",
      };
      this.props.client
        .mutate({
          mutation: createVisitor,
          variables: {
            id:this.props.navigation.state.params.mob,
            basicVisitor:
              Object.keys(basicVisitor).length > 0
                ? basicVisitor
                : undefined,
            jewExpo:jewExpo,
            exhName: 'jewExpo',
          },
        })
        .then(({ data }) => {
          console.log("data", data)
          this.setState({
            loading:false
          })
          Toast.show({
            text: 'Registered sucessfully',
            type: 'success',
            position: 'center',
            duration: 2000,
            style:{alignSelf:'center',width:wp('70%'), height:"auto",borderRadius:10,marginTop:20},
            textStyle: {
              textAlign: 'center',
              color:'black'
            }
          });
          AsyncStorage.setItem('user', JSON.stringify(data.createVisitor));
            this.props.navigation.navigate('Dashbord')
        })
        .catch(err => {
          console.log(`generateOtp : in err: ${JSON.stringify(err)}`);
        });

    }

  }

  render() {
    if (!this.state.isConnected) {
      return <Text>></Text>;
    }
console.log('ss',this.props.navigation.state.params.mob)
    return (
    
      <Container style={{ backgroundColor: '#e5e5e5' }}>
        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 50 }}>
          <Icon1 onPress={() => { this.props.navigation.navigate('Dashbord') }} style={{ width:wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
          <Text style={{ fontSize: 24, alignSelf: 'center',width:wp('80%'),textAlign:'center' }}>Registration</Text>
        </View>
        {this.state.loading ?
       <Spinner style={{alignSelf:'center',flex:1}} color='blue' />
      :
        <Content  >
        <Text style={{margin:5,color:'red'}}>This mobile number is not registered, Please create an account</Text>
          <View style={{ margin: 15 }}>
            <Form>
              <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginBottom: 10,width:wp('91%') }}>

                <View>
                  <Label style={{ marginBottom: 7 }}>First Name<Text style={{ color: "red" }}>*</Text></Label>
                  <Input style={{ backgroundColor: 'white', width: wp('45%') }}
                    placeholder='first name'
                    value={this.state.fName}
                    onChangeText={data =>
                      this.setState({ fName: data, error: false })
                    }
                  />
                </View>

                <View>
                  <Label style={{ marginBottom: 7 }}>Last Name<Text style={{ color: "red" }}>*</Text></Label>
                  <Input style={{ backgroundColor: 'white', width: wp('45%') }}
                    placeholder='last name'
                    value={this.state.lName}
                    onChangeText={data =>
                      this.setState({ lName: data, error: false })
                    }
                  />
                </View>

              </View>
              {this.state.error && this.state.fName == '' && this.state.lName == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter  Name...{' '}
                </Text>
                : <Text></Text>
              }

              <Label style={{ marginBottom: 7 }}>Company Name <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='company name'
                value={this.state.companyName}
                onChangeText={data =>
                  this.setState({ companyName: data, error: false })
                }
              />
              {this.state.error && this.state.companyName == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter Company Name...{' '}
                </Text>
                : <Text></Text>
              }

              <Label style={{ marginBottom: 7 }}>Designation <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='Designation'
                value={this.state.designation}
                onChangeText={data =>
                  this.setState({ designation: data, error: false })
                }
              />
              {this.state.error && this.state.designation == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter Designation...{' '}
                </Text>
                : <Text></Text>
              }

<Content style={{ margin: 10 }}>
                <Label style={{ marginBottom: 7 }}>Gender <Text style={{ color: "red" }}>*</Text></Label>
                <Grid>
                  <Col style={{ width: 60 }}>
                    <Text>Male</Text>
                    <Radio
                      color={"#f0ad4e"}
                      selectedColor={"#5cb85c"}
                      selected={this.state.gender == "MALE" && true}
                      onPress={() => this.setState({ gender: "MALE" })}
                    />
                  </Col>
                  <Col style={{ width: 60 }}>
                    <Text>Female</Text>
                    <Radio
                      color={"#f0ad4e"}
                      selectedColor={"#5cb85c"}
                      selected={this.state.gender == "FEMALE" && true}
                      onPress={() => this.setState({ gender: "FEMALE" })}
                    />
                  </Col>
                </Grid>
              </Content>

              <Label style={{ marginBottom: 7 }}>Member Photo <Text style={{ color: "red" }}>*</Text></Label>
                  {this.state.profile1 != '' && (
                <Card rounded style={styles.editStyle}>
                  <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
                    <Image
                      source={{ uri: this.state.profile1 }}
                      style={{ width: 70, height: 80 }}
                    />
                    <View
                      style={{
                        position: 'absolute',
                        right: 3,
                        top: 2,
                        height: 20,
                        width: 20,
                        backgroundColor: '#ccc',
                        alignItems: 'center',
                        borderRadius: 10,
                        justifyContent: 'center',
                      }}>
                      <TouchableWithoutFeedback onPress={() => this.delete()}>
                        <Text style={{ fontSize: 14 }}>X</Text>
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </Card>
              )}
             {this.state.profile1 == '' && (
                <>
                <Card style={styles.editStyle}>
                  <View style={{justifyContent:'center',alignItems:'center',flex:1}} >
                    <View style={{}}>
                      <TouchableWithoutFeedback
                        onPress={this.selectPhotoTapped.bind(this)}>
                        <Text style={styles.fabIcon}>+</Text>
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </Card>
                </>
              )}

{this.state.error && this.state.profilePhoto == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Member Photo...{' '}
                </Text>
                : <Text></Text>
              }
              <Label style={{ marginBottom: 7 }}>Upload Visiting Card <Text style={{ color: "red" }}>*</Text></Label>
{this.state.profile != '' && (
                <Card  style={styles.editStyle}>
                  <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
                    <Image
                      source={{ uri: this.state.profile }}
                      style={{ width: 70, height: 80 }}
                    />
                    <View
                      style={{
                        position: 'absolute',
                        right: 3,
                        top: 2,
                        height: 20,
                        width: 20,
                        backgroundColor: '#ccc',
                        alignItems: 'center',
                        borderRadius: 10,
                        justifyContent: 'center',
                      }}>
                      <TouchableWithoutFeedback onPress={() => this.delete1()}>
                        <Text style={{ fontSize: 14 }}>X</Text>
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </Card>
              )}
              {this.state.profile == '' && (
                <Card  style={styles.editStyle}>
                  <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
                   
                    <View style={{}}>
                      <TouchableWithoutFeedback
                        onPress={this.selectPhotoTapped1.bind(this)}>
                        <Text style={styles.fabIcon}>+</Text>
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </Card>
              )}
              {this.state.error && this.state.profilePhoto1 == '' ?
              <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                Upload Visiting Card...{' '}
              </Text>
              : <Text></Text>
            }

         <Text></Text>
              <Label style={{ marginBottom: 7, }}>Address Line 1 <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='Address Line 1'
                value={this.state.add1}
                onChangeText={data =>
                  this.setState({ add1: data, error: false })
                }
              />
              {this.state.error && this.state.add1 == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter Address Line 1...{' '}
                </Text>
                : <Text></Text>
              }
              <Label style={{ marginBottom: 7 }}>Address Line 2 <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='Address Line 2'
                value={this.state.add2}
                onChangeText={data =>
                  this.setState({ add2: data, error: false })
                }
              />
              {this.state.error && this.state.add2 == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter Address Line 2...{' '}
                </Text>
                : <Text></Text>
              }
              <Label style={{ marginBottom: 7 }}>City <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='City'
                value={this.state.city}
                onChangeText={data =>
                  this.setState({ city: data, error: false })
                }
              />
              {this.state.error && this.state.city == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter City...{' '}
                </Text>
                : <Text></Text>
              }

              <Label style={{ marginBottom: 7 }}>State <Text style={{ color: "red" }}>*</Text></Label>
              <Picker
                selectedValue={this.state.selectedValue}
                style={{ backgroundColor: 'white' }}
                onValueChange={(itemValue, itemIndex) => this.setState({ selectedValue: itemValue })}
              >
                {Object.keys(stateOptions)
                  .sort()
                  .map((state, i) => {
                    return (
                      <Picker.Item label={state} value={state} >
                        {stateOptions[state]}
                      </Picker.Item>
                    );
                  })}
              </Picker>
              {this.state.error && this.state.selectedValue == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter State ...{' '}
                </Text>
                : <Text></Text>
              }
              <Label style={{ marginBottom: 7 }}>Pin <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='pin'
                value={this.state.pin}
                onChangeText={data =>
                  this.setState({ pin: data, error: false })
                }
              />
              {this.state.error && this.state.pin == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter pin...{' '}
                </Text>
                : <Text></Text>
              }
              <Label style={{ marginBottom: 7 }}>Country Code <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='Country Code'
                value={this.state.ccode}
                onChangeText={data =>
                  this.setState({ ccode: data, error: false })
                }
              />
              {this.state.error && this.state.ccode == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter Country Code...{' '}
                </Text>
                : <Text></Text>
              }
              <Label style={{ marginBottom: 7 }}>Email <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='Email'
                value={this.state.email}
                onChangeText={data =>
                  this.setState({ email: data, error: false })
                }
              />
              {this.state.error && this.state.email == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter Email...{' '}
                </Text>
                : <Text></Text>
              }
              <Label style={{ marginBottom: 7 }}>GST Number <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='GST Number'
                value={this.state.gst}
                onChangeText={data =>
                  this.setState({ gst: data, error: false })
                }
              />
              {this.state.error && this.state.gst == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter GST Number...{' '}
                </Text>
                : <Text></Text>
              }

              <Label style={{ marginBottom: 7 }}>Telephone <Text style={{ color: "red" }}>*</Text></Label>
              <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                placeholder='Telephone'
                pattern="[1-9]{1}[0-9]{9}"
                maxLength={10}
                value={this.state.Telephone}
                onChangeText={data =>
                  this.setState({ Telephone: data, error: false })
                }
              />

              {this.state.error && this.state.Telephone == '' ?
                <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                  Please Enter  Number...{' '}
                </Text>
                : <Text></Text>
              }


              <Label style={{ marginBottom: 7, marginTop: 15 }}>Product interested in</Label>
              <View style={{ justifyContent: 'flex-start', flexDirection: 'row', width: wp('91%'), alignItems: 'center', alignContent: 'center', flexWrap: 'wrap' }}>
              {this.state.arr.map((i, j) => {
                  return (
                    <View style={{ justifyContent: 'flex-start', flexDirection: 'row', width: 300, marginTop: 5, marginBottom: 5, }}>
                      <CheckBox  onPress={() => this.toggleCheckbox(i.id)}
                        style={{ borderRadius: 5 }} checked={i.check} color="#B88A1C" />
                      <Text style={{ marginLeft: 15,fontWeight: i.check ? 'bold' : 'null' }}>{i.name}</Text>
                    </View>
                  )
                })}

              </View>
          

              <Button onPress={this.handleSubmit} style={{ marginTop: 10, backgroundColor: '#B88A1C' }} full ><Text>submit</Text></Button>

            </Form>
          </View>
        </Content>
      }
      </Container>
  );
  }
}

const styles = StyleSheet.create({

  img: {
    height: 20, width: 20, backgroundColor: 'yellow'
  },
  header_img: {
    height: 90, width: 90
  },
  viewCardmap: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
    margin: 10,
    flexWrap: 'wrap',
    display: 'flex',
    flex: 1
  },
  editStyle: {
    backgroundColor: '#fff',
    height:130,
    width:120,
    borderRadius:10
  },
  fabIcon: {
    alignSelf: 'center',
    fontSize: 70,
    color: '#ccc',
  },
  imageWrap: {
    margin: 20,
    padding: 2,
    height: Dimensions.get('window').height / 5 - 4,
    width: Dimensions.get('window').width / 3.5 - 4,
    flexDirection: 'column',
  },


});


export default withApollo(Register)
// this.state.productsInterestedIn.length > 0
// ? this.state.productsInterestedIn
// : undefined,