
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Picker,
    Dimensions,
    TouchableWithoutFeedback,
    PermissionsAndroid,
    AsyncStorage,
    ScrollView,
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import Icon1 from 'react-native-vector-icons/AntDesign'
import getJewExhProductsByExhId from '../GraphQl/getJewExhProduct'
import getProdAccessRequestById from '../GraphQl/getProdAccessRequestById'
import createProdAccessRequest from '../GraphQl/createProdAccessRequest'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Item, Input, Icon, Label, Textarea, Form, Spinner, Toast,CheckBox
} from 'native-base';
import ImageZoom from 'react-native-image-pan-zoom';
import configVariables from '../config/AppSync'
import Filter from '../Public/icon/Filter.svg'
const { width } = Dimensions.get('window');
const height = width * 0.8;
const images = [
    {
        source: require('../Public/img/Product_Search_img_1.png')
    },
    {
        source: require('../Public/img/Product_Search_img_1.png')
    }, {
        source: require('../Public/img/Product_Search_img_1.png')
    },

]

class ViewProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: true,
            loading: false,
            data: [],
            arr:[],
            id:'',
            requestStatus: "",
            requestButton:false,
            accessRequest:[],
            userName:'',
            companyName:'',
            city:''
        };
    }

    createDatasource = (data) => {
        let arr = []
        if (data != null) {
            data.map((i, j) => {
                let obj = {
                    accessType:i.accessType,
                    caption:i.caption,
                    category:i.category,
                    createdAt:i.createdAt,
                    description:i.description,
                    enabled:i.enabled,
                    exhId:i.exhId,
                    id:i.id,
                    image:i.image,
                    prodId:i.prodId,
                    checked:false,
                    imageArray:[],
                    imageSelect:false,
                    exhProps:{}
                }
              arr.push(obj)
            })
            console.log('arrr', arr)
            this.setState({
                data: arr
            })
        }
    }

    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });
let exh=this.props.navigation.state.params.exh
this.setState({
    exhProps:exh
})

AsyncStorage.getItem('user')
.then(data => {
  let data1 = JSON.parse(data);
    console.log("ss",data1)
  if (data1 != null) {
    this.setState({
      id:data1.id,
      userName:data1.basicVisitor.fName,
      city:data1.basicVisitor.address.city,
      companyName:data1.basicVisitor.companyName
    })
  } else {
  }
})
.catch(err => console.log('Error in login page', err));

        this.runQuery()
    }

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }

    runQuery = () => {
        console.log(this.props.navigation.state.params.exh.id)
        this.setState({
            loading: true,
        });

        this.props.client
            .query({
                query: getJewExhProductsByExhId,
                fetchPolicy: 'no-cache',
                variables: {
                    exhId: this.props.navigation.state.params.exh.id,
                },
            })
            .then(({ data }) => {
                console.log("data", data)
                if (data && data.getJewExhProductsByExhId) {
                    this.setState({
                        loading: false,
                    },()=>{this.createDatasource(data.getJewExhProductsByExhId)})
                    this.requestQuery()
                }
            })
            .catch((err) => {
                console.log(`Error : runQuery : in err: ${JSON.stringify(err)}`);
            });
    };
    confirmRequest = () => {
        
        this.props.client
          .mutate({
            mutation: createProdAccessRequest,
            variables: {
              exhId:this.props.navigation.state.params.exh.id,
              visitorId:this.state.id,
              name:this.state.userName,
              companyName:this.state.companyName,
              city:this.state.city,
              status: "NEW",
            },
          })
          .then((data) => {
              console.log("data......",data)
            Toast.show({
                text: 'Request For Full Catalogue is sent successfully',
                type: 'success',
                position: 'center',
                duration: 2000,
                style:{alignSelf:'center',width:wp('70%'), height:"auto",borderRadius:10,marginTop:20},
                textStyle: {
                  textAlign: 'center',
                  color:'black'
                }
              });
            this.setState({
              requestStatus: data.data.createProdAccessRequest.status,
              requestButton:true,
            });
          })
          .catch((err) => {
            console.log(err);
          });
      };

    checkProductGallery=()=>{
        console.log("checkProductGallery click.....................",this.state.exhProps)
        this.props.navigation.navigate('ProtectedProducts', {
            exh:this.state.exhProps,
          });

    }

 
    requestQuery = () => {
        this.props.client
          .query({
            query: getProdAccessRequestById,
            fetchPolicy: 'no-cache',
            variables: {
              exhId:this.props.navigation.state.params.exh.id,
               visitorId: this.state.id,
            },
          })    
          .then(({ data }) => {
            console.log("getProdAccessRequestById",data)
            if (data && data.getProdAccessRequestById) {
              this.setState({
                loading: false,
                accessRequest: data.getProdAccessRequestById,
                requestStatus: data.getProdAccessRequestById.status,
              });
              if (
                data.getProdAccessRequestById.status == "APPROVED" ||
                data.getProdAccessRequestById.status == "CANCELLED" ||
                data.getProdAccessRequestById.status == "NEW"
              ){
                this.setState({
                  requestButton: true,
                });
              }
            }
          })
          .catch((err) => {
            console.log(`Error : reqQuery : in err: ${JSON.stringify(err)}`);
          });
      };

    handleSubmit = () => {
        let arr = [],imageSelect=false;

        this.state.data.map((i, j) => {
            if (i.checked == true) {
                imageSelect=true
                let obj = {
                    image: i.image
                }
                arr.push(obj)
            }
        })
        console.log("arrrr",arr)
        console.log("imageSelect",imageSelect)
        if(imageSelect==true){
            this.props.navigation.navigate('EnquiryImage', {
                exh:this.state.exhProps,
                image:arr
              });
        }
        else{
            Toast.show({
                text: 'Please Select Images to go Forward !',
                type: 'danger',
                position: 'center',
                duration: 5000,
                style:{alignSelf:'center',width:wp('70%'), height:"auto",borderRadius:10,marginTop:20},
                textStyle: {
                  textAlign: 'center',
                //   color:'black'
                }
              });
              return false
            
        }

        this.setState({
                   imageArray:arr         
        })

    }

    toggleCheckbox=(id)=>{

        let changedCheckbox = ''
        changedCheckbox = this.state.data.find((cb) => cb.id === id);
        changedCheckbox.checked = !changedCheckbox.checked;
        console.log("c", changedCheckbox)
        const data = Object.assign([], this.state.data, changedCheckbox);
        this.setState({ data });
    }

    render() {
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }
        console.log('exh.......lllllllllllllll...',this.state.data.length)



        return (

            <Container style={{ backgroundColor: '#fff' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 50, backgroundColor: '#e5e5e5' }}>
                    <Icon1 onPress={() => { this.props.navigation.navigate(this.props.navigation.state.params && this.props.navigation.state.params.navigate1 && this.props.navigation.state.params.navigate1==1?'ViewExhibitor':'Manufacturers') }} style={{ width: wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                    <Text style={{ fontSize: 24, alignSelf: 'center', textAlign: 'center', width: wp('80%') }}>View Product</Text>
                </View>

                <Content>
                    <View>
                        <ScrollView
                            horizontal
                            pagingEnabled
                            showsHorizontalScrollIndicator={false}
                        >
                            {this.state.data.length >= 1 && this.state.data.map((product) => {
                                return (
                                    <>
                                        {product.accessType == "VIEW_ALL" &&
                                            <View >
                                                <Text
                                                    style={{
                                                        fontWeight: "bold",
                                                        color: "#C4941C",
                                                        fontSize: 19,
                                                        textAlign: 'center',
                                                        marginBottom: 10,
                                                        marginTop: 10
                                                    }}
                                                >
                                                    {product.category}
                                                </Text>
                                                <ImageZoom cropWidth={width}
                                                        cropHeight={350}
                                                        imageWidth={width}
                                                        imageHeight={350}>
                                                <Image
                                                    style={
                                                        styles.image
                                                    }
                                                    resizeMode='contain'
                                                    source={{
                                                        uri: `https://${product.image.bucket}.s3.${product.image.region}.amazonaws.com/${product.image.key}`,
                                                    }}
                                                />
                                                </ImageZoom>
                                                <Text
                                                    style={{
                                                        fontWeight: "bold",
                                                        color: "#C4941C",
                                                        fontSize: 14,
                                                        textAlign: 'center',
                                                        width:wp('90%'),
                                                        marginTop: 10

                                                    }}
                                                >
                                                    {product.caption}
                                                </Text>
                                                <View style={{marginTop:20,justifyContent:'flex-start',flexDirection:'row',alignItems:'center',alignContent:'center',marginLeft:15}}>
                                               <Text>Select :</Text>
                                                <CheckBox  style={{marginLeft:15}}
                                                 onPress={() => this.toggleCheckbox(product.id)} 
                                                 style={{ borderRadius: 5 }} checked={product.checked}
                                                  color="#B88A1C" />
                                                    </View>

                                                <View style={{ justifyContent: 'space-between', width: wp('95%'), flexDirection: 'row', alignSelf: 'center',marginTop:20 }}>
                                                    <Button style={{  width: wp('40%'), backgroundColor: '#C4941C',borderRadius:10 }}
                                                        onPress={this.handleSubmit} full>
                                                        <Text>Send Enquiry</Text>
                                                    </Button>
                                                {this.state.requestStatus == "APPROVED" ?
                                                    <Button
                                                    onPress={this.checkProductGallery}
                                                        style={{
                                                            color: "white",
                                                            backgroundColor: "green",
                                                            width: wp('50%'),
                                                            borderRadius: 10
                                                        }}
                                                    >
                                                        <Text style={{textAlign:'center'}}>
                                                            Check Product Gallery
                                                       </Text>
                                                     </Button>
                                                        :
                                                    <Button 
                                                    onPress={this.confirmRequest}
                                                    disabled={
                                                        this.state.requestStatus == "CANCELLED"?
                                                        true
                                                        : this.state.requestStatus == "NEW"?
                                                        true
                                                        :
                                                        false
                                                    }
                                                     style={{ width: wp('50%'),
                                                      backgroundColor: 
                                                      this.state.requestStatus == "APPROVED"
                                                      ? "green"
                                                      : this.state.requestStatus == "CANCELLED"
                                                      ? "grey"
                                                      : this.state.requestStatus == "NEW"
                                                      ? "grey" :
                                                      '#C4941C', 
                                                      borderRadius: 10 }}
                                                        full>
                                                        <Text style={{textAlign:'center'}}>
                                                            {this.state.requestStatus == "APPROVED"
                                                                ? "Request Approved"
                                                                : this.state.requestStatus == "CANCELLED"
                                                                    ? "Wait For Approval"
                                                                    : this.state.requestStatus == "NEW"
                                                                        ? "Pending Approval"
                                                                        : "Request Full Catalogue"}
                                                            </Text>
                                                    </Button>
                                                }
                                                </View>


                                            </View>
                                        }
                                    </>
                                )
                            })}

                        </ScrollView>
                    </View>
                </Content>
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        height: 90, width: 90
    },
    scrollContainer: {
        height,
    },
    image: {
        width,
        height: 350,
        // resizeMode: 'stretch'
    },

});

export default withApollo(ViewProduct)

// <Content>
// <View style={{ flex: 1 }} onLayout={this._onLayoutDidChange}>

//     <Carousel
//         delay={2000}
//         style={this.state.size}
//         autoplay={false}
//         arrows={true}
//         leftArrowText='<<'
//         rightArrowText='>>'
//         pageInfo
//         onAnimateNextPage={(p) => console.log(p)}
//     >
//         {this.state.data.length > 0 && this.state.data.map((product, j) => {
//             return (

//                 <View >
//                     <Text
//                         style={{
//                             fontWeight: "bold",
//                             color: "#C4941C",
//                             fontSize: 19,
//                             textAlign: 'center',
//                             marginBottom: 10,
//                             marginTop: 10
//                         }}
//                     >
//                         {product.category}
//                     </Text>
//                     <ImageZoom cropWidth={width}
//                         cropHeight={350}
//                         imageWidth={width}
//                         imageHeight={350}>




//                         <Image
//                             style={
//                                 styles.image
//                             }
//                             resizeMode='contain'
//                             source={{
//                                 uri: `https://${product.image.bucket}.s3.${product.image.region}.amazonaws.com/${product.image.key}`,
//                             }}
//                         />
//                     </ImageZoom>
//                     <Text
//                         style={{
//                             fontWeight: "bold",
//                             color: "#C4941C",
//                             fontSize: 14,
//                             textAlign: 'center',
//                             width: wp('90%'),
//                             marginTop: 10

//                         }}
//                     >
//                         {product.caption}
//                     </Text>
//                     <View style={{ marginTop: 20, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center', alignContent: 'center', marginLeft: 15 }}>
//                         <Text>Select :</Text>
//                         <CheckBox style={{ marginLeft: 15 }}
//                             onPress={() => this.toggleCheckbox(product.id)}
//                             style={{ borderRadius: 5 }} checked={product.checked}
//                             color="#B88A1C" />
//                     </View>

//                     <View style={{ justifyContent: 'space-between', width: wp('95%'), flexDirection: 'row', alignSelf: 'center', marginTop: 20 }}>
//                         <Button style={{ width: wp('40%'), backgroundColor: '#C4941C', borderRadius: 10 }}
//                             onPress={this.handleSubmit} full>
//                             <Text>Send Enquiry</Text>
//                         </Button>
//                         {this.state.requestStatus == "APPROVED" ?
//                             <Button
//                                 onPress={this.checkProductGallery}
//                                 style={{
//                                     color: "white",
//                                     backgroundColor: "green",
//                                     width: wp('50%'),
//                                     borderRadius: 10
//                                 }}
//                             >
//                                 <Text style={{ textAlign: 'center' }}>
//                                     Check Product Gallery
//                                    </Text>
//                             </Button>
//                             :
//                             <Button
//                                 onPress={this.confirmRequest}
//                                 disabled={
//                                     this.state.requestStatus == "CANCELLED" ?
//                                         true
//                                         : this.state.requestStatus == "NEW" ?
//                                             true
//                                             :
//                                             false
//                                 }
//                                 style={{
//                                     width: wp('50%'),
//                                     backgroundColor:
//                                         this.state.requestStatus == "APPROVED"
//                                             ? "green"
//                                             : this.state.requestStatus == "CANCELLED"
//                                                 ? "grey"
//                                                 : this.state.requestStatus == "NEW"
//                                                     ? "grey" :
//                                                     '#C4941C',
//                                     borderRadius: 10
//                                 }}
//                                 full>
//                                 <Text style={{ textAlign: 'center' }}>
//                                     {this.state.requestStatus == "APPROVED"
//                                         ? "Request Approved"
//                                         : this.state.requestStatus == "CANCELLED"
//                                             ? "Wait For Approval"
//                                             : this.state.requestStatus == "NEW"
//                                                 ? "Pending Approval"
//                                                 : "Request Full Catalogue"}
//                                 </Text>
//                             </Button>
//                         }
//                     </View>


//                 </View>

//             )
//         })}
//     </Carousel>
// </View>
// </Content>