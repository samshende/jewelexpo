import React, {Component} from "react";
import {StyleSheet, View, SafeAreaView} from "react-native";
import shortid from "shortid";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {Autocomplete, withKeyboardAwareScrollView} from "react-native-dropdown-autocomplete";

 class HomeScreen extends Component {

  handleSelectItem(item, index) {
    const {onDropdownClose} = this.props;
    onDropdownClose();
    console.log(item);
  }

  render() {
    const autocompletes = [...Array(10).keys()];
    console.log("auto",autocompletes)

    const apiUrl = "https://5b927fd14c818e001456e967.mockapi.io/branches";

    const {scrollToInput, onDropdownClose, onDropdownShow} = this.props;

    return (
      <View style={styles.autocompletesContainer}>
            <Autocomplete
               placeholder='select company                                                                                                       '
               initialValue='sam'
               key={shortid.generate()}
              style={styles.input}
              scrollToInput={ev => scrollToInput(ev)}
              handleSelectItem={(item, id) => this.handleSelectItem(item, id)}
              onDropdownClose={() => onDropdownClose()}
              onDropdownShow={() => onDropdownShow()}
              fetchDataUrl={apiUrl}
              minimumCharactersCount={3}
              highlightText
              valueExtractor={item => item.name}
            />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  autocompletesContainer: {
    paddingTop: 0,
    zIndex: 1,
    width: wp('95%'),
    alignSelf:'center',
  },
  input: {
    maxWidth:wp('91%'),
    alignSelf:'center'
  },


});
export default withKeyboardAwareScrollView(HomeScreen)