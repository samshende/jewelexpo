
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";

import Icon1 from 'react-native-vector-icons/AntDesign'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Icon, Text, View, Card, Left, Right, Thumbnail, Body, Item, Input, Label
} from 'native-base';
import Home from '../Public/icon/Home.svg'
import Enquiry from '../Public/icon/enquiry.svg'
import Notification from '../Public/icon/Notification_yellow.svg'
import Profile from '../Public/icon/profile.svg'
import Filter from '../Public/icon/Filter.svg'
let img = [
    {
        img: require('../Public/img/noti_img_1.png'),
    },
    {
        img: require('../Public/img/noti_img_2.png'),
    },
    {
        img: require('../Public/img/noti_img_3.png'),
    }
]

let arr = [
    {
        date: '3 june 2020, 5.05 PM',
        info: 'Filo Diamonds Organize Jewellery Exhibition on 09 April 2020 ,Booking Now',
        info1: 'Alyana Thomson Send Enquiry for Kalyan Jewellers',
        img: require('../Public/img/notification_1.png'),

    },
    {
        date: '3 june 2020, 5.05 PM',
        info: null,
        info1: 'Kate Morrison Canceled Your Filo Diamond Appointment',
        img: require('../Public/img/notification_2.png'),
    },
    {
        date: '3 june 2020, 5.05 PM',
        info: null,
        info1: 'Alyana Thomson Send Enquiry for Kalyan Jewellers',
        img: require('../Public/img/notification_3.png'),
    },

]

export default class Notification1 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: true
        };
    }

    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });
    }

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }

    render() {
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }

        return (
            <Container style={{ backgroundColor: '#e5e5e5' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 40 }}>
                    <Icon1 onPress={() => { this.props.navigation.navigate('Dashbord') }} style={{ width: wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                    <Text style={{ fontSize: 24, alignSelf: 'center', textAlign: 'center', width: wp('80%') }}>Notification</Text>
                </View>
                <Header searchBar rounded style={{ backgroundColor: '#e5e5e5', elevation: 0 }}>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Search here...." />
                        <Filter style={{ marginRight: 10 }} />
                    </Item>
                </Header>
                <View
                    style={{
                        flex: 1,
                        justifyContent: "center",
                        alignItems: "center",
                    }}>
                        <Text style={{fontSize:24,fontWeight:'bold',color:'#C4941C'}}>Coming Soon</Text>

                </View>

                {/* <Content>
                    <View style={{ }}>
                        {arr.map((i,j)=>{
                            {console.log(i)}
                            return(
                                <Card style={{ width: wp('92%'), height: 'auto',alignSelf:'center' }}>
                                <View style={{ margin: 7 }}>
                                 {(j==0 || j==2) &&   <View style={{position:'absolute',height:10,width:10,borderRadius:10/2,backgroundColor:'#9D323D',left:0,}}/>}
                                    <View style={styles.viewCardmap}>
                                        <Thumbnail style={{  }}  source={i.img} />
                                        <View style={{marginLeft:10,flex:1}}>
                                       {i.info!=null &&     <Text style={{color:'#8e8c96',}}>{i.info}</Text>}
                                            <Text style={{color:'#8e8c96'}}>{i.info1}</Text>
                                            {j==0 &&
                                            <View style={{flexDirection:'row'}}>
                                            { img.map((i,j)=>{
                                                return(
                                                    <Image
                                                    style={styles.header_img}
                                                    source={i.img}
                                                />
                                                )
                                            }) }
                                            </View>
                                            }
                                        </View>
                                    </View>
                                    <Text style={{ textAlign: 'right', bottom: 0, marginBottom: 5,color:'#6b6e6e' }}>{i.date}</Text>
                                  {j==2 &&   <Text style={{position:'absolute',left:0,bottom:0,color:'#8e8c96'}}>1 more notification</Text>}
                                </View>
                            </Card>
                            )
                        })}
                       
                    </View>
                </Content>
             */}
                <Footer style={{ borderTopLeftRadius: 60 }}>
                    <FooterTab style={{ backgroundColor: '#2E2D33' }}>

                        <Button vertical onPress={() => { this.props.navigation.navigate('Dashbord') }} >
                            <Home  height={25} width={25}/>
                            <Text style={{ fontSize: 8 }}>Home</Text>
                        </Button>

                        <Button vertical onPress={() => { this.props.navigation.navigate('Approvedexhibitors') }}>
                        <Enquiry height={25} width={25} />
                        <Text style={{ fontSize: 8 }}>My List</Text>
                    </Button>

                        <Button vertical >
                            <Notification height={25} width={25} />
                            <Text style={{ fontSize: 8 }}>Notifications</Text>
                        </Button>

                        <Button vertical onPress={() => { this.props.navigation.navigate('Profile') }}>
                            <Profile height={25} width={25} />
                            <Text style={{ fontSize: 8 }}>My Profile</Text>
                        </Button>
                    </FooterTab>

                </Footer>

            </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        alignSelf: 'center',
        height: 50,
        width: 50,
        margin: 5
    },
    viewCardmap: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        marginLeft: 20
    },


});
