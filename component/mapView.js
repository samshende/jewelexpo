
import React, { Component } from 'react';
import { View, StyleSheet } from 'react-native';
import { Container, Header, Content, Footer, FooterTab, Button, Text, Form, Item, Input, Label, Textarea, Picker, Toast, Spinner } from 'native-base';
import MapView, { PROVIDER_GOOGLE, Marker } from 'react-native-maps';
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import { requestLocationPermission } from '../config/permissions';
import Geolocation from '@react-native-community/geolocation';
class GoogleCustMap extends React.Component {
  constructor(props){
    super(props)
    this.state={
      loading: false,
      location: {
        latitude: 17.8990583,
        longitude: 74.8252976,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      },
      markers:{
        latitude: 17.8990583,
        longitude: 74.8252976,
        latitudeDelta: 0.015,
        longitudeDelta: 0.0121,
      },
      first:false
    }
  }

 
  requestPermission = async(type)=>{
    let flag = false;
    let res = await requestLocationPermission();
console.log("flag",res)
    if(res == 'GRANTED'){
        flag = true
    }
console.log("flag",flag)
    if(flag == true){
        await this.fetchCurrentLocation()
    }
}



fetchCurrentLocation=()=>{
  this.getCurrentPosition()
}

getCurrentPosition=()=>{
  Geolocation.getCurrentPosition(
    position => {
      const initialPosition = position;
      if(position && position != null){
        console.log(position)
        let obj = {
          latitude:position.coords.latitude,
          longitude:position.coords.longitude,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }
    
        this.setState({markers:obj,loading:obj});
      }
    })
  }





  render() {
console.log(this.props.data)
    return (
      <View>
      <View style={styles.container}>
        <MapView
          provider={PROVIDER_GOOGLE} // remove if not using Google Maps
          style={styles.map}
          region={this.props.data}
        
            onPress={()=>{
             console.log("sam")
            }
        }
        >
              <MapView.Marker coordinate={this.props.data} title={this.props.city}
              description={this.props.companyName}>
                <MapView.Callout tooltip >
               
                </MapView.Callout>
                </MapView.Marker>
          </MapView>
          </View>
        </View>
   
    );
  }
}


const styles = StyleSheet.create({
  container: {
    height: 550,
    width: 'auto',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
 });




  export default GoogleCustMap;