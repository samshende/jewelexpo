
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Picker,
    Dimensions,
    TouchableWithoutFeedback,
    PermissionsAndroid,
    AsyncStorage
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import ImagePicker from 'react-native-image-picker';
import Icon1 from 'react-native-vector-icons/AntDesign'
import createEnquiryToExhibitor from '../GraphQl/createEnquiryToExhibitor'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Item, Input, Icon, Label, Textarea, Form, Spinner,Toast,
} from 'native-base';
import configVariables from '../config/AppSync'
import Filter from '../Public/icon/Filter.svg'
const requestCameraPermission = async () => {
    try {
        const granted = await PermissionsAndroid.request(
            PermissionsAndroid.PERMISSIONS.CAMERA,
            {
                title: "Cool Photo App Camera Permission",
                message:
                    "Cool Photo App needs access to your camera " +
                    "so you can take awesome pictures.",
                buttonNeutral: "Ask Me Later",
                buttonNegative: "Cancel",
                buttonPositive: "OK"
            }
        );
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
            console.log("You can use the camera");
        } else {
            console.log("Camera permission denied");
        }
    } catch (err) {
        console.log(err);
    }
};


class Enquiry extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName: '',
            error: false,
            city: '',
            Enquiry: '',
            number: '',
            address: '',
            companyName: '',
            profile1: '',
            profilePhoto: '',
            proFileName: '',
            isConnected: true,
            loading: false
        };
    }

    enterLoading = () => {
        this.setState({
            loading: true
        })
    }
    handleSubmit = () => {
        this.enterLoading()
        if (this.state.companyName == '' || this.state.userName == '' || this.state.address == '' || this.state.number == '' || this.state.Enquiry == '' || this.state.city == '') {
            this.setState({
                error: true,
                loading: false
            })
            // return
        }
        else {

            // console.log('1',this.state)

            // return

            this.props.client
                .mutate({
                    mutation: createEnquiryToExhibitor,
                    variables: {
                        userMob:this.props.navigation.state.params.exh.id,
                        salesHead:this.props.navigation.state.params.exh.salesHead,
                          email:this.props.navigation.state.params.exh.email,
                        visitorCompanyName:this.state.companyName,
                        visitorName:this.state.userName,
                        visitorMob:this.state.number,
                        visitorEmail: this.state.address,
                        enquiryDetails:this.state.Enquiry
                    },
                })
                .then(({ data }) => {
                    console.log("data", data)
                    this.setState({
                        loading: false
                    })
                    Toast.show({
                        text: 'Enquiry is sent successfully',
                        type: 'success',
                        position: 'center',
                        duration: 5000,
                        style:{alignSelf:'center',width:wp('70%'), height:"auto",borderRadius:10,marginTop:20},
                        textStyle: {
                          textAlign: 'center',
                          color:'black'
                        }
                      });
                })
                .catch(err => {
                    console.log(`generateOtp : in err: ${JSON.stringify(err)}`);
                });

        }
    }


    oncityChange(value) {
        console.log('value :: ', value);
        this.setState({
            city: value,
        });
        // console.log('IN city Change :: ', this.state.city);
    }


    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });
        AsyncStorage.getItem('user')
        .then(data => {
          let data1 = JSON.parse(data);
          if (data1 != null) {
            // console.log("user",data1)
            this.setState({
                userName:data1.basicVisitor && data1.basicVisitor.fName?data1.basicVisitor.fName: '',
            companyName:data1.basicVisitor && data1.basicVisitor.companyName?data1.basicVisitor.companyName: '',
            number:data1.id,
            address:data1.basicVisitor && data1.basicVisitor.email?data1.basicVisitor.email: '',
            city:data1.basicVisitor && data1.basicVisitor.address &&  data1.basicVisitor.address.city?data1.basicVisitor.address.city: '',
            
            })
        }
    })
    }

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }
    render() {
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }

// console.log("state",this.state)
// console.log("props",this.props.navigation.state.params.exh)

        return (
   
                    <Container style={{ backgroundColor: '#e5e5e5' }}>
                        <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 40 }}>
                            <Icon1 onPress={() => { this.props.navigation.navigate(this.props.navigation.state.params
                                 && this.props.navigation.state.params.navigate1 
                                 && this.props.navigation.state.params.navigate1==1
                                 ?'ViewExhibitor'
                                 :
                                 this.props.navigation.state.params
                                 && this.props.navigation.state.params.navigate1 
                                 && this.props.navigation.state.params.navigate1==2?
                                 'SearchProduct'
                                 :
                                 this.props.navigation.state.params
                                 && this.props.navigation.state.params.navigate1 
                                 && this.props.navigation.state.params.navigate1==3?
                                 'Approvedexhibitors'
                                 :
                                 'Manufacturers') }} style={{ width:wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                            <Text style={{ fontSize: 24, alignSelf: 'center',textAlign:'center',width:wp('80%') }}>Send Enquiry</Text>
                        </View>
                        { this.state.loading ?
             <Spinner style={{alignSelf:'center',flex:1}} color='blue' />
            :
                        <Content>
                            <View style={{ margin: 15 }}>
                                <Form>
                                    <Label style={{ marginBottom: 7 }}>Company Name <Text style={{ color: "red" }}>*</Text></Label>
                                    <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                        placeholder='company name'
                                        disabled={true}
                                        value={this.state.companyName}
                                        onChangeText={data =>
                                            this.setState({ companyName: data, error: false })
                                        }
                                    />
                                    {this.state.error && this.state.companyName == '' ?
                                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                            Please Enter Company Name...{' '}
                                        </Text>
                                        :<View></View>
                                    }

                                    <Label style={{ marginBottom: 7, marginTop: 15 }}>Contact Person Name<Text style={{ color: "red" }}>*</Text></Label>
                                    <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                        placeholder='name'
                                        disabled={true}
                                        value={this.state.userName}
                                        onChangeText={data =>
                                            this.setState({ userName: data, error: false })
                                        }
                                    />
                                    {this.state.error && this.state.userName == '' &&
                                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                            Please Enter Person Name...{' '}
                                        </Text>
                                    }


                                    <Label style={{ marginBottom: 7, marginTop: 15 }}>Email Address<Text style={{ color: "red" }}>*</Text></Label>
                                    <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                        placeholder='email'
                                        disabled={true}
                                        value={this.state.address}
                                        onChangeText={data =>
                                            this.setState({ address: data, error: false })
                                        }
                                    />
                                    {this.state.error && this.state.address == '' &&
                                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                            Please Enter Email Address...{' '}
                                        </Text>
                                    }


                                    <Label style={{ marginBottom: 7, marginTop: 15 }}>Phone Number<Text style={{ color: "red" }}>*</Text></Label>
                                    <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                        placeholder='number'
                                        disabled={true}
                                        value={this.state.number}
                                        pattern="[1-9]{1}[0-9]{9}"
                                        maxLength={10}
                                        onChangeText={data =>
                                            this.setState({ number: data, error: false })
                                        }
                                    />
                                    {this.state.error && this.state.number == '' &&
                                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                            Please Enter Phone Number...{' '}
                                        </Text>
                                    }


                                    <Label style={{ marginBottom: 7, marginTop: 15 }}>City<Text style={{ color: "red" }}>*</Text></Label>
                                    <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                        placeholder='city'
                                        value={this.state.city}
                                        disabled={true}
                                        onChangeText={data =>
                                            this.setState({ city: data, error: false })
                                        }
                                    />
                                    {this.state.error && this.state.city == '' &&
                                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                            Please Enter City...{' '}
                                        </Text>
                                    }

                                    <Label style={{ marginBottom: 7, marginTop: 15 }}>Enquiry Details<Text style={{ color: "red" }}>*</Text></Label>
                                    <Textarea rowSpan={5} bordered
                                        style={{ backgroundColor: 'white', width: wp('91%') }}
                                        placeholder='Enquiry Details'
                                        value={this.state.Enquiry}
                                        onChangeText={data =>
                                            this.setState({ Enquiry: data, error: false })
                                        }
                                    />
                                    {this.state.error && this.state.Enquiry == '' &&
                                        <Text style={{ color: 'red', marginLeft: 20, marginTop: 3 }}>
                                            Please Enter Enquiry Details...{' '}
                                        </Text>
                                    }


                                    <Button style={{ marginTop: 10, backgroundColor: '#B88A1C' }} onPress={this.handleSubmit} full><Text>Send Enquiry</Text></Button>
                                </Form>
                            </View>
                        </Content>
                        }
                    </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        height: 90, width: 90
    },
    viewCardmap: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center',
        alignItems: 'center',
        margin: 10,
        flexWrap: 'wrap',
        display: 'flex',
        flex: 1
    },
    editStyle: {
        backgroundColor: '#fff',
        marginTop: 20,
        flexDirection: 'row',
        alignItems: 'center',
    },
    fabIcon: {
        alignSelf: 'center',
        fontSize: 70,
        color: '#ccc',
    },
    imageWrap: {
        margin: 20,
        padding: 2,
        height: Dimensions.get('window').height / 5 - 4,
        width: Dimensions.get('window').width / 3.5 - 4,
        flexDirection: 'column',
    },

});

export default withApollo(Enquiry)