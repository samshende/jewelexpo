import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Picker,
    Dimensions,
    TouchableWithoutFeedback,
    PermissionsAndroid,
    AsyncStorage,
    ScrollView,
    Linking
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import Icon1 from 'react-native-vector-icons/AntDesign'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Item,
     Input, Icon, Label, Textarea, Form, Spinner, Toast,CheckBox,Tabs,Tab
} from 'native-base';
import configVariables from '../config/AppSync'
import Message from '../Public/icon/message.svg'
import Callender from '../Public/icon/callender.svg'
import Product from '../Public/icon/Product_view.svg'
const { width } = Dimensions.get('window');
const height = width * 0.8;
const images = [
    {
        source: require('../Public/img/Product_Search_img_1.png')
    },
    {
        source: require('../Public/img/Product_Search_img_1.png')
    }, {
        source: require('../Public/img/Product_Search_img_1.png')
    },

]

const stateOptions = {
    // "ALL_STATES": "All States",
    JAMMU_KASHMIR: "Jammu & Kashmir",
    HIMACHAL_PRADESH: "Himachal Pradesh",
    PUNJAB: "Punjab",
    DELHI: "Delhi",
    RAJASTHAN: "Rajasthan",
    UTTARAKHAND: "Uttarakhand",
    UTTAR_PRADESH: "Uttar Pradesh",
    WEST_BENGAL: "West Bengal",
    BIHAR: "Bihar",
    JHARKHAND: "Jharkhand",
    ODISHA: "Odisha",
    ASSAM: "Assam",
    ARUNACHAL_PRADESH: "Arunachal Pradesh",
    MEGHALAYA: "Meghalaya",
    TRIPURA: "Tripura",
    MIZORAM: "Mizoram",
    MANIPUR: "Manipur",
    NAGALAND: "Nagaland",
    MAHARASHTRA: "Maharashtra",
    GUJARAT: "Gujarat",
    GOA: "Goa",
    CHHATTISGARH: "Chhattisgarh",
    MADHYA_PRADESH: "Madhya Pradesh",
    ANDHRA_PRADESH: "Andhra Pradesh",
    HARYANA: "Haryana",
    KERALA: "Kerala",
    SIKKIM: "Sikkim",
    TAMIL_NADU: "Tamil Nadu",
    TELANGANA: "Telangana",
    KARNATAKA: "Karnataka",
  };

class ViewExhibitor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            exhProps: this.props.navigation.state.params.exh,
            product:[],
            stateArr:'',
            arr :[
                {
                 name:'Gold Chain',
                 val:'GOLD_CHAINS',
                 check:false,
                 id:1
                },
                {
                 name:'Mangalsutras',
                 val:'MANGALSUTRAS',
                 check:false,
                 id:2
                },
                {
                 name:'Bangles',
                 val:'BANGLES',
                 check:false,
                 id:3
                },
                {
                 name:'Bracelets',
                 val:'BRACELETS',
                 check:false,
                 id:4
                },
                {
                 name:'Casting jewellery',
                 val:'CASTING_JEWELLERY',
                 check:false,
                 id:5
                },
                {
                 name:'CZ Diamond jewellery',
                 val:'CZ_DIAMOND_JEWELLERY',
                 check:false,
                 id:6
                },
                {
                 name:'Antique jewellery',
                 val:'ANTIQUE_JEWELLERY',
                 check:false,
                 id:7
                },
                {
                 name:'Temple jewellery',
                 val:'TEMPLE_JEWELLERY',
                 check:false,
                 id:8
                },
                {
                 name:'Kolkata jewellery',
                 val:'KOLKATA_JEWELLERY',
                 check:false,
                 id:9
                },
                {
                 name:'Imported italian jewellery',
                 val:'IMPORTED_ITALIAN_JEWELLERY',
                 check:false,
                 id:10
                },
                {
                 name:'Lightweight jewellery',
                 val:'LIGHTWEIGHT_JEWELLERY',
                 check:false,
                 id:11
                },
                {
                 name:'Designer antique jewellery',
                 val:'DESIGNER_ANTIQUE_JEWELLERY',
                 check:false,
                 id:12
                },
                {
                 name:'Plain gold jewellery',
                 val:'PLAIN_GOLD_JEWELLERY',
                 check:false,
                 id:13
                },
                {
                 name:'real diamond jewellery',
                 val:'REAL_DIAMOND_JEWELLERY',
                 check:false,
                 id:14
                },
                {
                 name:'Platinum jewellery',
                 val:'PLATINUM_JEWELLERY',
                 check:false,
                 id:15
                },
                {
                 name:'925 Silver jewellery',
                 val:'SILVER_JEWELLERY_925',
                 check:false,
                 id:16
                },
                {
                 name:'Silver articles',
                 val:'SILVER_ARTICLES',
                 check:false,
                 id:17
                },
                {
                 name:'Loose diamonds',
                 val:'LOOSE_DIAMONDS',
                 check:false,
                 id:18
                },
                {
                 name:'Diamond jewellery',
                 val:'DIAMOND_JEWELLERY',
                 check:false,
                 id:19
                },
                {
                 name:'Gold jewellery',
                 val:'GOLD_JEWELLERY',
                 check:false,
                 id:20
                },
                {
                 name:'Jadau jewellery',
                 val:'JADAU_JEWELLERY',
                 check:false,
                 id:21
                },  
                {
                 name:'Silver jewellery',
                 val:'SILVER_JEWELLERY',
                 check:false,
                 id:22
                },
                {
                 name:'Machinery allied section',
                 val:'MACHINERY_ALLIED_SECTION',
                 check:false,
                 id:23
                },
                {
                 name:'Rose Gold Jewellery',
                 val:'ROSE_GOLD_JEWELLERY',
                 check:false,
                 id:24
                },
                {
                 name:'Rajkot Jewellery',
                 val:'RAJKOT_JEWELLERY',
                 check:false,
                 id:25
                },
                {
                 name:'Coimbatore Jewellery',
                 val:'COIMBATORE_JEWELLERY',
                 check:false,
                 id:26
                },
                {
                 name:'Kids Jewellery',
                 val:'EMERALD_JEWELLERY',
                 check:false,
                 id:27
                },
                {
                 name:'Emerald Jewellery',
                 val:'EMERALD_JEWELLERY',
                 check:false,
                 id:28
                },
                {
                 name:'CNC Mala',
                 val:'CNC_MALA',
                 check:false,
                 id:29
                },
                {
                 name:'Gemstones',
                 val:'GEMSTONES',
                 check:false,
                 id:30
                },
                {
                 name:'Emerald',
                 val:'EMERALD',
                 check:false,
                 id:31
                },
                {
                 name:'Gold Mountings',
                 val:'GOLD_MOUNTINGS',
                 check:false,
                 id:32
                },
                {
                 name:'Turkish Jewellery',
                 val:'TURKISH_JEWELLERY',
                 check:false,
                 id:33
                },
                {
                 name:'Nose Pins',
                 val:'NOSE_PINS',
                 check:false,
                 id:34
                },
             ]
        };
    }

    sendEnquiry=(data)=>{
        if(data!=null){
            let exh={
                id:data.id?data.id:'',
                salesHead:data.salesHead?data.salesHead:'',
                email:data.email?data.email:''
            }
        console.log("data e",exh)
            // return
            this.props.navigation.navigate('Enquiry', {
                exh:exh,
                navigate1:1
              });
        }

    }
    bookAppointment=(data)=>{
        console.log("data e",data)
        if(data!=null){
            let exh={
                id:data.id?data.id:'',
                companyName:data.companyName?data.companyName:''
            }
        console.log("data e",exh)
        // return
            this.props.navigation.navigate('BookAppointment', {
                exh:exh,
                navigate1:1
              });
        }

    }

    ViewProduct=(data)=>{
        console.log("data e",data)
        if(data!=null){
            let exh={
                id:data.id?data.id:'',
                companyName:data.companyName?data.companyName:'',
                salesHead:data.salesHead?data.salesHead:'',
                email:data.email?data.email:'',
                businessAddress:data.businessAddress?data.businessAddress:'',
                logo:data.logo?data.logo:''
            }
        console.log("data e",exh)
        // return
            this.props.navigation.navigate('ViewProduct', {
                exh:exh,
                navigate1:1
              });
        }

    }


    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });
let exh=this.props.navigation.state.params.exh
let arr=[],stateArr=[]
exh.productsDealingIn && exh.productsDealingIn.length>0 && exh.productsDealingIn.map((i,j)=>{
    let a=i
    this.state.arr.map((i,j)=>{
        if(a==i.val){
            arr.push(i.name)
        }

    })
})
exh.target_states && exh.target_states.length>0 && exh.target_states.map((i,j)=>{
    let a=i
{Object.keys(stateOptions)
    .sort()
    .map((state, i) => {
        if(a==state){
            stateArr.push(stateOptions[state])
        }
          
    })}
})
let strArr=stateArr.join(', ')
console.log("sammm", strArr)
console.log("rrrrr",arr)
this.setState({
    product:arr,
    stateArr:strArr
})
}

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }
  
renderWhatsapp=()=>{
    console.log("render whatsapp")
    Linking.openURL(`whatsapp://send?text=hello&phone=91${this.state.exhProps.id ? this.state.exhProps.id :''}`)
}
    render() {
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }
        console.log('exh..........',this.state.exhProps)

        return (

            <Container style={{ backgroundColor: '#e5e5e5' }}>
                 <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 40 }}>
                            <Icon1 onPress={() => { this.props.navigation.navigate(
                                 this.props.navigation.state.params
                                 && this.props.navigation.state.params.navigate1 
                                 && this.props.navigation.state.params.navigate1==2
                                 ?
                                 'SearchProduct'
                                 :
                                 'Manufacturers') 
                                 }} style={{ width:wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                            <Text style={{ fontSize: 24, alignSelf: 'center',textAlign:'center',width:wp('80%') }}>Company Profile</Text>
                        </View>
                <Content style={{ backgroundColor: '#e5e5e5', }}>
                    <Card style={{borderColor:'black',borderWidth:5,}}>
                    <View style={{marginTop:15}}>
                        <View style={{position:'absolute',flexDirection:'row',justifyContent:'flex-start',marginRight:15,alignContent:'center',alignItems:'center',right:0,top:0,marginTop:-10,
                    }}>
                        <Text style={{alignSelf:'center'}}>Mobile:{this.state.exhProps.id ? this.state.exhProps.id :''}</Text>
                        <TouchableWithoutFeedback onPress={this.renderWhatsapp}>
                        <Image
                        style={{height:25,width:25,alignSelf:'center',marginLeft:10}}
                        source={require('../Public/img/whatsapp.png')}
                        />
                        </TouchableWithoutFeedback>
                    </View>
                    {this.state.exhProps.logo && this.state.exhProps.logo.bucket!=null ?
                        <Image
                            style={{
                                position:'absolute',
                                margin:5,
                                top: 0,
                                left: 0,
                                width: 70,
                                height:70,
                                resizeMode:'center',
                                marginTop:10
                                
                            }}
                            source={{
                                uri: `https://${this.state.exhProps.logo.bucket}.s3.${this.state.exhProps.logo.region}.amazonaws.com/${this.state.exhProps.logo.key}`,
                            }}
                        />
                        :
                        <Image
                        style={{ position:'absolute',
                        margin:5,
                        top: 0,
                        left: 0,
                        width: 70,
                        height:70,
                        resizeMode:'center',
                        marginTop:10
                     }}
                        source={require('../Public/img/blank.png')}
                    />
                    }

                    <Text style={{textAlign:'left',marginLeft:100,height:70,flexWrap:'wrap',marginTop:20,
                      fontWeight: "bold",color: "#C4941C", fontSize: 15}}>
                    {this.state.exhProps.companyName?this.state.exhProps.companyName:''}{'\n'}
                    <Text style={{fontWeight:'300',color:'#6e706f'}}>
                        ({this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.city
                                : ""}{"-"}
                                  {this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.zip
                                : ""}
                                )
                    </Text>
                    </Text>
                    </View>
                    <Card style={{width:wp('95%'),borderWidth:5,alignSelf:'center',borderColor:'black',borderColor:'#C4941C'}}>
                        <View style={{margin:5,marginBottom:10}}>
                    <Text style={{fontWeight:'bold',color: "#C4941C",}}>Dealing in:</Text>
                    {this.state.product.length>0 && this.state.product.map((i,j)=>{
                        return(
                    <View style={{ flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center', alignContent: 'center', flexWrap: 'wrap' }}>
                         <View style={{ height: 12, width: 12, backgroundColor: '#C4941C' }} />
                            <Text style={{ marginLeft: 5,color:'#6e706f' }}>{i}</Text>
                     </View>
                        )
                    })}

                    <Text style={{fontWeight:'bold',color: "#C4941C",marginTop:10}}>Jewellery Melting:</Text>
                    {this.state.exhProps.productsMelting && this.state.exhProps.productsMelting.length>0 && this.state.exhProps.productsMelting.map((i,j)=>{
                        return(
                            <Text style={{color:'#6e706f'}}>{i}</Text>
                        )
                    })}
                    </View>
                    </Card>

                    <Card style={{width:wp('95%'),borderWidth:5,alignSelf:'center',borderColor:'black',borderColor:'#C4941C'}}>
                        <View style={{margin:5,marginBottom:10}}>
                    <Text style={{fontWeight:'bold',color: "#C4941C",marginTop:20}}>Company Profile:</Text>
                    <Text style={{color:'#6e706f'}}>{this.state.exhProps.moreProductDetails ? this.state.exhProps.moreProductDetails : ''}</Text>

                    <Text style={{fontWeight:'bold',color: "#C4941C",marginTop:20}}>Target States:</Text>
                    <Text style={{color:'#6e706f'}}>{this.state.stateArr}</Text>
                    </View>
                    </Card>

                    <Card style={{width:wp('95%'),borderWidth:5,alignSelf:'center',borderColor:'black',borderColor:'#C4941C'}}>
                        <View style={{margin:5,marginBottom:10}}>

                    <Text style={{fontWeight:'bold',color: "#C4941C",marginTop:20,fontSize:22}}>Contact Details:</Text>

                    <Text style={{fontWeight:'bold',color: "#C4941C",marginTop:20}}>Owner/Director Name:</Text>
                    <Text style={{color:'#6e706f'}}>{this.state.exhProps.ownerName ? this.state.exhProps.ownerName :''}</Text>

                    <Text style={{fontWeight:'bold',color: "#C4941C",marginTop:20}}>Sales Contact Person:</Text>
                    <Text style={{color:'#6e706f'}}>{this.state.exhProps.salesHead ? this.state.exhProps.salesHead :''}</Text>

                    <Text style={{fontWeight:'bold',color: "#C4941C",marginTop:20}}>City:</Text>
                    <Text style={{color:'#6e706f'}}> {this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.city
                                : ""}
                        </Text>

                    <Text style={{fontWeight:'bold',color: "#C4941C",marginTop:20}}>Business Address:</Text>
                        <Text style={{marginBottom: 40,color:'#6e706f' }}>
                            {this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.addressLineOne
                                : ""}
                            ,{" "}
                            {this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.addressLineTwo
                                : ""}
                            ,
                              {this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.city
                                : ""}{" "}
                            {this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.state
                                : ""}{" "}
                            {this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.country
                                : ""}{" "}
                            -{" "}
                            {this.state.exhProps.businessAddress
                                ? this.state.exhProps.businessAddress.zip
                                : ""}
                        </Text>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between',width:wp('80%'),alignSelf:'center',  bottom: 10, alignContent:'center',alignItems:'center' }}>
                                                <Callender onPress={()=>this.bookAppointment(this.state.exhProps)} style={{ }} width={40} height={45} />
                                                <Message onPress={()=>{this.sendEnquiry(this.state.exhProps)}} width={40} height={45} />
                                                <Product onPress={()=>{this.ViewProduct(this.state.exhProps)}}  style={{ }} width={47} height={47} />
                                            </View>
                        </View>
                        </Card>

                    {/* <Card style={{width:wp('95%'),borderWidth:5,alignSelf:'center',borderColor:'black'}}>
                        <View style={{margin:5}}>
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between',width:wp('80%'),alignSelf:'center',  bottom: 0, alignContent:'center',alignItems:'center' }}>
                                                <Callender onPress={()=>this.bookAppointment(this.state.exhProps)} style={{ }} width={40} height={45} />
                                                <Message onPress={()=>{this.sendEnquiry(this.state.exhProps)}} width={40} height={45} />
                                                <Product onPress={()=>{this.ViewProduct(this.state.exhProps)}}  style={{ }} width={47} height={47} />
                                            </View>
                    </View>
                    </Card> */}
                    </Card>
                </Content>
                    </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        height: 90, width: 90
    },
    scrollContainer: {
        height,
    },
    image: {
        width:wp('85%'),
        height: 350,
        resizeMode: 'stretch'
    },

});

export default withApollo(ViewExhibitor)

