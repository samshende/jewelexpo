
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Picker,
    Dimensions,
    TouchableWithoutFeedback,
    PermissionsAndroid,
    AsyncStorage,
    ScrollView,
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import Icon1 from 'react-native-vector-icons/AntDesign'
import getJewExhProductsByExhId from '../GraphQl/getJewExhProduct'
import getProdAccessRequestById from '../GraphQl/getProdAccessRequestById'
import createProdAccessRequest from '../GraphQl/createProdAccessRequest'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import ImageZoom from 'react-native-image-pan-zoom';
import RNPickerSelect from 'react-native-picker-select';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Item,
    Input, Icon, Label, Textarea, Form, Spinner, Toast, CheckBox, Tabs, Tab
} from 'native-base';
import PhotoView from 'react-native-photo-view-ex';
import configVariables from '../config/AppSync'
import Filter from '../Public/icon/Filter.svg'
const { width } = Dimensions.get('window');
const height = width * 0.8;
const images = [
    {
        source: require('../Public/img/Product_Search_img_1.png')
    },
    {
        source: require('../Public/img/Product_Search_img_1.png')
    }, {
        source: require('../Public/img/Product_Search_img_1.png')
    },

]

const productCategory = [
    { value: "Antique Jewellery", label: "Antique Jewellery" },
    { value: "Casting Jewellery", label: "Casting Jewellery" },
    { value: "Coimbatore Jewellery", label: "Coimbatore Jewellery" },
    { value: "CZ Diamond Jewellery", label: "CZ Diamond Jewellery" },
    { value: "Designer Gold Jewellery", label: "Designer Gold Jewellery" },
    { value: "Diamond Jewellery", label: "Diamond Jewellery" },
    { value: "Emerald", label: "Emerald" },
    { value: "Gemstones", label: "Gemstones" },
    { value: "Gold Jewellery", label: "Gold Jewellery" },
    { value: "Gold Mountings", label: "Gold Mountings" },
    { value: "International Jewellery", label: "International Jewellery" },
    { value: "Jadau Jewellery", label: "Jadau Jewellery" },
    { value: "Kolkata Jewellery", label: "Kolkata Jewellery" },
    { value: "Machinery & Allied", label: "Machinery & Allied" },
    { value: "Plain Gold", label: "Plain Gold" },
    { value: "Platinum", label: "Platinum" },
    { value: "Rajkot Jewellery", label: "Rajkot Jewellery" },
    { value: "Rose Gold Jewellery", label: "Rose Gold Jewellery" },
    { value: "Silver", label: "Silver" },
    { value: "Temple Jewellery", label: "Temple Jewellery" },
    { value: "Turkish Jewellery", label: "Turkish Jewellery" },
];


class ProtectedProducts extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: true,
            loading: true,
            data: [],
            data1:[],
            arr: [],
            id: '',
            requestStatus: "",
            requestButton: false,
            accessRequest: [],
            userName: '',
            companyName: '',
            city: '',
            exhProps: this.props.navigation.state.params.exh,
            counter: 0,
            viewArr: [],
            protectedArr: [],
            categoryValue:''
        };
    }

    createDatasource = (data) => {
        let arr = [], arr1 = [], arr2 = []
        if (data != null) {
            data.map((i, j) => {
                let obj = {
                    accessType: i.accessType,
                    caption: i.caption,
                    category: i.category,
                    createdAt: i.createdAt,
                    description: i.description,
                    enabled: i.enabled,
                    exhId: i.exhId,
                    id: i.id,
                    image: i.image,
                    prodId: i.prodId,
                    checked: false,
                    imageArray: [],
                    imageSelect: false,
                    exhProps: {}
                }
                i.accessType == "VIEW_ALL" ? arr1.push(obj) : i.accessType == "ACCESS_CONTROLLED" ? arr2.push(obj) : ''

                arr.push(obj)
            })
            console.log('arrr', arr1, arr2)
            this.setState({
                loading: false,
                data: arr,
                data1: arr,
                viewArr: arr1,
                protectedArr: arr2
            })
        }
    }

    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });


        this.runQuery()
    }

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }

    runQuery = () => {
        console.log(this.props.navigation.state.params.exh.id)

        this.props.client
            .query({
                query: getJewExhProductsByExhId,
                fetchPolicy: 'no-cache',
                variables: {
                    exhId: this.props.navigation.state.params.exh.id,
                },
            })
            .then(({ data }) => {
                console.log("data.....................", data)
                if (data && data.getJewExhProductsByExhId) {
                    this.setState({
                    }, () => { this.createDatasource(data.getJewExhProductsByExhId) })
                }
            })
            .catch((err) => {
                console.log('Error : runQuery : in err: ', err);
            });
    };

    onChangeCategory = (value) => {
        console.log("sam", value)
        let arr = []
        this.state.data1.map((i, j) => {
            if (i.category.toLowerCase().includes(value.toLowerCase())) {
                arr.push(i)
            }
        })
        this.setState({
            data:arr
        })
        console.log('arrrrr', arr)
    }



    handleSubmit = () => {
        let arr = [], imageSelect = false;

        this.state.data.map((i, j) => {
            if (i.checked == true) {
                imageSelect = true
                let obj = {
                    image: i.image
                }
                arr.push(obj)
            }
        })
        console.log("arrrr", arr)
        console.log("imageSelect", imageSelect)
        // return
        if (imageSelect == true) {
            this.props.navigation.navigate('EnquiryImage', {
                exh: this.state.exhProps,
                image: arr,
                navigate1: 1
            });
        }
        else {
            Toast.show({
                text: 'Please Select Images to go Forward !',
                type: 'danger',
                position: 'center',
                duration: 5000,
                style: { alignSelf: 'center', width: wp('70%'), height: "auto", borderRadius: 10, marginTop: 20 },
                textStyle: {
                    textAlign: 'center',
                    //   color:'black'
                }
            });
            return false

        }

        this.setState({
            imageArray: arr
        })

    }

    toggleCheckbox = (id) => {
        let count = 0
        let changedCheckbox = ''
        changedCheckbox = this.state.data.find((cb) => cb.id === id);
        changedCheckbox.checked = !changedCheckbox.checked;
        console.log("c", changedCheckbox)
        const data = Object.assign([], this.state.data, changedCheckbox);
        data.map((i, j) => {
            if (i.checked == true) {
                count++
            }
        })
        console.log("count.......", count)
        this.setState({ data, counter: count });
    }

    render() {
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }
        console.log('exh..........', this.state.protectedArr.length)

        const placeholder = {
            label: 'Select a Category...',
            value: null,
            color: '#9EA0A4',
        };
        return (

            <Container style={{ backgroundColor: '#fff' }}>
                <Button style={{
                    position: 'absolute',
                    right: 30, bottom: 50,
                    zIndex: 200,
                    width: wp('40%'), backgroundColor: this.state.counter > 0 ? '#C4941C' : '#dec892', borderRadius: 10,
                }}
                    disabled={this.state.counter > 0 ? false : true}
                    onPress={this.handleSubmit} full>
                    {this.state.counter > 0 ?
                        <Text>
                            {this.state.counter}{' '}Send Enquiry
                                                        </Text>
                        :
                        <Text>
                            Send Enquiry
                                                        </Text>
                    }
                </Button>

                {/* style={{
                              position: 'absolute',
                              margin: "25px",
                              top: 0,
                              left: 0,
                              maxWidth: "120px",
                              height:'auto',
                              float:'left'
                            }} */}
                <View style={{ height: 'auto', backgroundColor: '#e5e5e5', }}>
                    <Icon1
                        onPress={() => {
                            this.props.navigation.navigate(
                                this.props.navigation.state.params
                                    && this.props.navigation.state.params.navigate1
                                    && this.props.navigation.state.params.navigate1 == 1
                                    ?
                                    'Approvedexhibitors'
                                    :
                                    this.props.navigation.state.params
                                        && this.props.navigation.state.params.navigate1
                                        && this.props.navigation.state.params.navigate1 == 5
                                        ?
                                        'Manufacturers'
                                        :
                                        'ViewProduct')
                        }}
                        style={{ alignSelf: 'center', position: 'absolute', top: 20, left: 10 }} name="arrowleft" size={25} />

                    <Image
                        style={{
                            width: 70,
                            height: 70,
                            resizeMode: 'center',
                            alignSelf: 'center'
                        }}
                        source={{
                            uri: `https://${this.state.exhProps.logo.bucket}.s3.${this.state.exhProps.logo.region}.amazonaws.com/${this.state.exhProps.logo.key}`,
                        }}
                    />
                    <Text style={{
                        alignSelf: 'center', textAlign: 'center',
                        fontWeight: "bold", color: "#C4941C", fontSize: 19, flexWrap: 'wrap'
                    }}>
                        {this.state.exhProps.companyName ? this.state.exhProps.companyName : 'dfghj'}
                    </Text>
                    <Text style={{ width: wp('95%'), alignSelf: 'center', textAlign: 'center', flexWrap: 'wrap', marginBottom: 20 }}>
                        {this.state.exhProps.businessAddress
                            ? this.state.exhProps.businessAddress.addressLineOne
                            : ""}
                        ,{" "}
                        {this.state.exhProps.businessAddress
                            ? this.state.exhProps.businessAddress.addressLineTwo
                            : ""}
                        ,
                              {this.state.exhProps.businessAddress
                            ? this.state.exhProps.businessAddress.city
                            : ""}{" "}
                        {this.state.exhProps.businessAddress
                            ? this.state.exhProps.businessAddress.state
                            : ""}{" "}
                        {this.state.exhProps.businessAddress
                            ? this.state.exhProps.businessAddress.country
                            : ""}{" "}
                        -{" "}
                        {this.state.exhProps.businessAddress
                            ? this.state.exhProps.businessAddress.zip
                            : ""}
                    </Text>

                </View>
                {this.state.loading ?
                    <Spinner style={{ alignSelf: 'center', flex: 1 }} color='blue' />
                    :
                    <Tabs onChangeTab={()=>{this.setState({data:this.state.data1})}} >

                        <Tab
                            tabStyle={{ backgroundColor: 'white' }}
                            activeTabStyle={{ backgroundColor: 'white' }}
                            textStyle={{ color: 'black', fontWeight: '500' }}
                            activeTextStyle={{ color: '#C4941C', fontWeight: '500' }}

                            heading="FREE ACCESS"
                        >
                            {this.state.viewArr.length == 0
                                ? <Text style={{
                                    fontWeight: "bold",
                                    color: "#C4941C",
                                    fontSize: 19,
                                    marginTop: 100,
                                    alignSelf: 'center'
                                }}
                                >Product Not Available!!
                            </Text>
                                :
                                <Content style={{ marginBottom: 20, marginTop: 20 }}>
                                    <RNPickerSelect
                                        placeholder={placeholder}
                                        style={styles}
                                        useNativeAndroidPickerStyle={false}
                                        onValueChange={(val)=>{this.onChangeCategory(val)}}
                                        items={productCategory}
                                    />
                                    <Button onPress={()=>{this.setState({data:this.state.data1})}} full style={{backgroundColor:'#C4941C',width:100,marginLeft:20,marginTop:10,marginBottom:10}}><Text>clear</Text></Button>

                                        {this.state.data.length==0 &&
                                        <Text style={{fontSize:20,fontWeight:'bold',alignSelf:'center',marginTop:30}}>
                                            No Products Found !
                                        </Text>
                                    }
                                    {this.state.data.length >0 && this.state.data.map((product) => {
                                        return (
                                            <>
                                                {product.accessType == "VIEW_ALL" &&
                                                    <Card style={{ width: wp('90%'), height: 490, borderColor: 'black', alignSelf: 'center', }}>

                                                        <View style={{ alignContent: 'center', alignItems: 'center', flex: 1 }} >

                                                            <Text
                                                                style={{
                                                                    fontWeight: "bold",
                                                                    color: "#C4941C",
                                                                    fontSize: 19,
                                                                    textAlign: 'center',
                                                                    marginBottom: 10,
                                                                    marginTop: 10
                                                                }}
                                                            >
                                                                {product.category}
                                                            </Text>
                                                            <Text style={{ position: 'absolute', bottom: 190, zIndex: 200, fontWeight: 'bold', backgroundColor: '#616061', color: 'white' }}>Double Tap to zoom</Text>
                                                            {/* <ImageZoom cropWidth={wp('85%')}
                                                        cropHeight={300}
                                                        imageWidth={wp('85%')}
                                                        imageHeight={300}>
                                                        <Image
                                                            style={
                                                                styles.image
                                                            }
                                                            resizeMode='contain'
                                                            source={{
                                                                uri: `https://${product.image.bucket}.s3.${product.image.region}.amazonaws.com/${product.image.key}`,
                                                            }}
                                                        />
                                                    </ImageZoom> */}
                                                            <PhotoView

                                                                zoomTransitionDuration={1000}
                                                                source={{ uri: `https://${product.image.bucket}.s3.${product.image.region}.amazonaws.com/${product.image.key}` }}
                                                                minimumZoomScale={1}
                                                                maximumZoomScale={3}
                                                                resizeMode="contain"
                                                                onLoad={() => console.log("Image loaded!")}
                                                                style={styles.image}
                                                            />
                                                            <Text
                                                                style={{
                                                                    fontWeight: "bold",
                                                                    color: "#C4941C",
                                                                    fontSize: 14,
                                                                    textAlign: 'center',
                                                                    width: wp('90%'),
                                                                    marginTop: 10

                                                                }}
                                                            >
                                                                {product.caption}
                                                            </Text>
                                                            <View style={{
                                                                marginTop: 20, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center',
                                                                alignContent: 'center', marginLeft: 15, position: 'absolute', bottom: 0, left: 0, marginBottom: 40
                                                            }}>
                                                                <Text>Select :</Text>
                                                                <CheckBox style={{ marginLeft: 15 }}
                                                                    onPress={() => this.toggleCheckbox(product.id)}
                                                                    style={{ borderRadius: 5 }} checked={product.checked}
                                                                    color="#B88A1C" />
                                                            </View>




                                                        </View>
                                                    </Card>
                                                }
                                            </>
                                        )
                                    })}
                                </Content>
                            }
                        </Tab>

                        <Tab
                            tabStyle={{ backgroundColor: 'white' }}
                            activeTabStyle={{ backgroundColor: 'white' }}
                            textStyle={{ color: 'black', fontWeight: '500' }}
                            activeTextStyle={{ color: '#C4941C', fontWeight: '500' }}
                            heading="PROTECTED"
                        >

                            {this.state.protectedArr.length == 0
                                ? <Text style={{
                                    fontWeight: "bold",
                                    color: "#C4941C",
                                    fontSize: 19,
                                    marginTop: 100,
                                    alignSelf: 'center'
                                }}
                                >Product Not Available!!</Text>
                                :
                                <Content style={{ marginBottom: 20, marginTop: 20 }}>
                                     <RNPickerSelect
                                        placeholder={placeholder}
                                        style={styles}
                                        useNativeAndroidPickerStyle={false}
                                        onValueChange={(val)=>{this.onChangeCategory(val)}}
                                        items={productCategory}
                                    />
                                    <Button onPress={()=>{this.setState({data:this.state.data1})}} full style={{backgroundColor:'#C4941C',width:100,marginLeft:20,marginTop:10,marginBottom:10}}><Text>clear</Text></Button>

                                        {this.state.data.length==0 &&
                                        <Text style={{fontSize:20,fontWeight:'bold',alignSelf:'center',marginTop:30}}>
                                            No Products Found !
                                        </Text>
                                    }

                                    {this.state.data.length >= 1 && this.state.data.map((product) => {
                                        return (
                                            <>
                                                {product.accessType == "ACCESS_CONTROLLED" &&
                                                    <Card style={{ width: wp('90%'), height: 490, borderColor: 'black', alignSelf: 'center', }}>

                                                        <View style={{ alignContent: 'center', alignItems: 'center', flex: 1 }} >

                                                            <Text
                                                                style={{
                                                                    fontWeight: "bold",
                                                                    color: "#C4941C",
                                                                    fontSize: 19,
                                                                    textAlign: 'center',
                                                                    marginBottom: 10,
                                                                    marginTop: 10
                                                                }}
                                                            >
                                                                {product.category}
                                                            </Text>
                                                            <Text style={{ position: 'absolute', bottom: 190, zIndex: 200, fontWeight: 'bold', backgroundColor: '#616061', color: 'white' }}>Double Tap to zoom</Text>
                                                            {/* <ImageZoom cropWidth={wp('85%')}
                                                        cropHeight={300}
                                                        imageWidth={wp('85%')}
                                                        imageHeight={300}>
                                                        <Image
                                                            style={
                                                                styles.image
                                                            }
                                                            resizeMode='contain'
                                                            source={{
                                                                uri: `https://${product.image.bucket}.s3.${product.image.region}.amazonaws.com/${product.image.key}`,
                                                            }}
                                                        />
                                                    </ImageZoom> */}
                                                            <PhotoView
                                                                zoomTransitionDuration={1000}
                                                                source={{ uri: `https://${product.image.bucket}.s3.${product.image.region}.amazonaws.com/${product.image.key}` }}
                                                                minimumZoomScale={1}
                                                                maximumZoomScale={3}
                                                                resizeMode="contain"
                                                                onLoad={() => console.log("Image loaded!")}
                                                                style={styles.image}
                                                            />
                                                            <Text
                                                                style={{
                                                                    fontWeight: "bold",
                                                                    color: "#C4941C",
                                                                    fontSize: 14,
                                                                    textAlign: 'center',
                                                                    width: wp('90%'),
                                                                    marginTop: 10

                                                                }}
                                                            >
                                                                {product.caption}
                                                            </Text>
                                                            <View style={{
                                                                marginTop: 20, justifyContent: 'flex-start', flexDirection: 'row', alignItems: 'center',
                                                                alignContent: 'center', marginLeft: 15, position: 'absolute', bottom: 0, left: 0, marginBottom: 40
                                                            }}>
                                                                <Text>Select :</Text>
                                                                <CheckBox style={{ marginLeft: 15 }}
                                                                    onPress={() => this.toggleCheckbox(product.id)}
                                                                    style={{ borderRadius: 5 }} checked={product.checked}
                                                                    color="#B88A1C" />
                                                            </View>




                                                        </View>
                                                    </Card>
                                                }
                                            </>
                                        )
                                    })}
                                </Content>
                            }
                        </Tab>
                    </Tabs>

                }
            </Container>
        );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        height: 90, width: 90
    },
    scrollContainer: {
        height,
    },
    image: {
        width: wp('85%'),
        height: 300,
        maxHeight: 300,
        maxWidth: wp('85%'),
        // resizeMode: 'stretch'
    },
    inputAndroid: {
        fontSize: 16,
        paddingHorizontal: 10,
        paddingVertical: 8,
        borderWidth: 0.5,
        borderColor: 'grey',
        borderRadius: 8,
        width:wp('80%'),
        marginLeft:20,
        color: 'black',
        paddingRight: 30, // to ensure the text is never behind the icon
    },

});

export default withApollo(ProtectedProducts)