
import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    Picker,
    Dimensions,
    TouchableWithoutFeedback,
    PermissionsAndroid,
    AsyncStorage
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import ImagePicker from 'react-native-image-picker';
import Icon1 from 'react-native-vector-icons/AntDesign'
import requestSiteAccessToExhibitor from '../GraphQl/createDemand'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Item, Input, Icon, Label,Textarea,Form,Spinner,
} from 'native-base';
import configVariables from '../config/AppSync'
import Filter from '../Public/icon/Filter.svg'
const requestCameraPermission = async () => {
  try {
    const granted = await PermissionsAndroid.request(
      PermissionsAndroid.PERMISSIONS.CAMERA,
      {
        title: "Cool Photo App Camera Permission",
        message:
          "Cool Photo App needs access to your camera " +
          "so you can take awesome pictures.",
        buttonNeutral: "Ask Me Later",
        buttonNegative: "Cancel",
        buttonPositive: "OK"
      }
    );
    if (granted === PermissionsAndroid.RESULTS.GRANTED) {
      console.log("You can use the camera");
    } else {
      console.log("Camera permission denied");
    }
  } catch (err) {
    console.log(err);
  }
};


 class PostDemand extends Component {
    constructor(props) {
        super(props);
        this.state = {
            userName:'',
            error:false,
            city:'',
            Enquiry:'',
            number:'',
            address:'',
            companyName:'',
            profile1: '',
      profilePhoto: '',
      proFileName: '',
      isConnected:true,
      loading:false
        };
    }

    enterLoading=()=>{
      this.setState({
        loading:true
      })
    }
    handleSubmit=()=>{
    this.enterLoading()
      if(this.state.companyName=='' || this.state.userName=='' || this.state.address=='' || this.state.number=='' || this.state.Enquiry=='' || this.state.city==''){
        this.setState({
          error:true,
        loading:false
        })
        // return
      }
      else {
        let   testphoto=this.state.profilePhoto != ''
        ?
       {
            bucket: configVariables.s3Bucket,
            region: configVariables.region,
            localUri: this.state.profilePhoto,
            key: `JewelExpo/Images/${this.state.proFileName}`,
            mimeType: 'image/jpeg',
          }
          :''
        console.log('1',testphoto)
  
        return
        
        this.props.client
          .mutate({
            mutation: requestSiteAccessToExhibitor,
            variables: {
              contactPerson:this.state.userName,
                          mob:this.state.number,
                          email:this.state.address,
                          city: this.state.city,
                          demanddetails: this.state.Enquiry,
                          demandphotos:testphoto,
                          
            },
          })
          .then(({ data }) => {
            console.log("data", data)
            this.setState({
              loading:false
            })
          })
          .catch(err => {
            console.log(`generateOtp : in err: ${JSON.stringify(err)}`);
          });
  
      }
    }

    
    oncityChange(value) {
        console.log('value :: ', value);
        this.setState({
          city: value,
        });
        // console.log('IN city Change :: ', this.state.city);
      }

      getBlob = response => {
        console.log('in getBlob ::', response);
        const xhr = new XMLHttpRequest();
        xhr.open('GET', response.uri, true);
        xhr.responseType = 'blob';
        xhr.onload = () => {
          const reader = new FileReader();
          reader.onloadend = () => {
            let myBase64 = reader.result;
            var buf = new Buffer(
              myBase64.replace(/^data:image\/\w+;base64,/, ''),
              'base64',
            );
    
            this.setState({
              profilePhoto: buf,
              proFileName: response.fileName,
            });
          };
          reader.readAsDataURL(xhr.response);
        };
        xhr.send();
      };

      delete = () => {
        this.setState({
          proFileName: '',
          profile1: '',
          profilePhoto: '',
        });
      };
      selectPhotoTapped() {
        requestCameraPermission()
        const options = {
          quality: 1.0,
          maxWidth: 500,
          maxHeight: 500,
          title: 'Select Photo',
          storageOptions: {
            skipBackup: true,
          },
        };
    
        ImagePicker.showImagePicker(options, response => {
          console.log('Response showImagePicker = ', response);
          if (response.didCancel) {
            console.log('User cancelled photo picker');
          } else if (response.error) {
            console.log('ImagePicker Error: ', response.error);
          } else if (response.customButton) {
            console.log('User tapped custom button: ', response.customButton);
          } else {
            let source = { uri: response.uri, fileName: response.fileName };
            this.getBlob(source);
            this.setState({
              profile1: `data:image/png;base64,${response.data}`,
            });
          }
        });
      }


      componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
          });
          AsyncStorage.getItem('user')
          .then(data => {
            let data1 = JSON.parse(data);
            if (data1 != null) {
              // console.log("user",data1)
              this.setState({
                  userName:data1.basicVisitor && data1.basicVisitor.fName?data1.basicVisitor.fName: '',
              companyName:data1.basicVisitor && data1.basicVisitor.companyName?data1.basicVisitor.companyName: '',
              number:data1.id,
              address:data1.basicVisitor && data1.basicVisitor.email?data1.basicVisitor.email: '',
              city:data1.basicVisitor && data1.basicVisitor.address &&  data1.basicVisitor.address.city?data1.basicVisitor.address.city: '',
              
              })
          }
      })
      }
    
      componentWillMount() {
        NetInfo.fetch().then(isConnected => {
          this.setState({isConnected});
        });
    }
    render() {
      if (!this.state.isConnected) {
        return <Text>></Text>;
      }
  
   
        return (
            <Container style={{ backgroundColor: '#e5e5e5' }}>
                <View style={{ flexDirection: 'row', justifyContent: 'flex-start', height: 50 }}>
                    <Icon1 onPress={() => { this.props.navigation.navigate('Dashbord') }} style={{ width:wp('10%'), alignSelf: 'center', marginLeft: 5 }} name="arrowleft" size={20} />
                    <Text style={{ fontSize: 24, textAlign: 'center',alignSelf:'center', width:wp('80%') }}>Post Demand</Text>
                </View>
                {/* <Header searchBar rounded style={{ backgroundColor: '#e5e5e5', elevation: 0 }}>
                    <Item>
                        <Icon name="ios-search" />
                        <Input placeholder="Search here...." />
                        <Filter style={{ marginRight: 10 }} />
                    </Item>
                </Header> */}
                {this.state.loading ?
           <Spinner style={{alignSelf:'center',flex:1}} color='blue' />
          :
                <Content>
                    <View style={{ margin: 15 }}>
                        <Form>
                        <Label style={{ marginBottom: 7 }}>Company Name <Text style={{ color: "red" }}>*</Text></Label>
                        <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                        disabled={true}
                         placeholder='company name' 
                         value={this.state.companyName}
                      onChangeText={data =>
                        this.setState({companyName: data, error: false})
                      }
                      />
                       {this.state.error && this.state.companyName==''?
                    <Text style={{color: 'red', marginLeft: 20,marginTop:3}}>
                      Please Enter Company Name...{' '}
                    </Text>
                    :<View></View>
                  }

                        <Label style={{ marginBottom: 7,marginTop:15 }}>Contact Person Name<Text style={{ color: "red" }}>*</Text></Label>
                        <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                        disabled={true}
                         placeholder='name' 
                         value={this.state.userName}
                      onChangeText={data =>
                        this.setState({userName: data, error: false})
                      }
                      />
                       {this.state.error &&this.state.userName=='' && 
                        <Text style={{color: 'red', marginLeft: 20,marginTop:3}}>
                      Please Enter Person Name...{' '}
                    </Text>
                  }


                        <Label style={{ marginBottom: 7,marginTop:15 }}>Email Address<Text style={{ color: "red" }}>*</Text></Label>
                        <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                        disabled={true}
                         placeholder='email' 
                         value={this.state.address}
                      onChangeText={data =>
                        this.setState({address: data, error: false})
                      }
                      />
                       {this.state.error &&this.state.address=='' && 
                        <Text style={{color: 'red', marginLeft: 20,marginTop:3}}>
                      Please Enter Email Address...{' '}
                    </Text>
                  }


                        <Label style={{ marginBottom: 7,marginTop:15 }}>Phone Number<Text style={{ color: "red" }}>*</Text></Label>
                        <Input style={{ backgroundColor: 'white', width: wp('91%') }} 
                        disabled={true}
                        placeholder='number' 
                        pattern="[1-9]{1}[0-9]{9}"
                        maxLength={10}
                        value={this.state.number}
                      onChangeText={data =>
                        this.setState({number: data, error: false})
                      }
                      />
                       {this.state.error && this.state.number=='' && 
                        <Text style={{color: 'red', marginLeft: 20,marginTop:3}}>
                      Please Enter Phone Number...{' '}
                    </Text>
                  }


                        <Label style={{ marginBottom: 7,marginTop:15 }}>City<Text style={{ color: "red" }}>*</Text></Label>
                        <Input style={{ backgroundColor: 'white', width: wp('91%') }}
                                        placeholder='city'
                                        value={this.state.city}
                                        disabled={true}
                                        onChangeText={data =>
                                            this.setState({ city: data, error: false })
                                        }
                                    />
                    {this.state.error && this.state.city=='' && 
                        <Text style={{color: 'red', marginLeft: 20,marginTop:3}}>
                      Please Enter City...{' '}
                    </Text>
                  }

             <Label style={{ marginBottom: 7,marginTop:15 }}>Enquiry Details<Text style={{ color: "red" }}>*</Text></Label>
             <Textarea rowSpan={5} bordered
                         style={{ backgroundColor: 'white', width: wp('91%') }} 
                        placeholder='Enquiry Details' 
                        value={this.state.Enquiry}
                      onChangeText={data =>
                        this.setState({Enquiry: data, error: false})
                      }
                      />
                       {this.state.error && this.state.Enquiry=='' && 
                        <Text style={{color: 'red', marginLeft: 20,marginTop:3}}>
                      Please Enter Enquiry Details...{' '}
                    </Text>
                  }

<Label style={{ marginBottom: 7 }}>Upload Images <Text style={{ color: "red" }}>*</Text></Label>
                  {this.state.profile1 != '' && (
                <Card rounded style={styles.editStyle}>
                  <View style={{justifyContent:'center',alignItems:'center',flex:1}}>
                    <Image
                      source={{ uri: this.state.profile1 }}
                      style={{ width: 70, height: 80 }}
                    />
                    <View
                      style={{
                        position: 'absolute',
                        right: 3,
                        top: 2,
                        height: 20,
                        width: 20,
                        backgroundColor: '#ccc',
                        alignItems: 'center',
                        borderRadius: 10,
                        justifyContent: 'center',
                      }}>
                      <TouchableWithoutFeedback onPress={() => this.delete()}>
                        <Text style={{ fontSize: 14 }}>X</Text>
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </Card>
              )}
             {this.state.profile1 == '' && (
                <>
                <Card style={styles.editStyle}>
                  <View style={{justifyContent:'center',alignItems:'center',flex:1}} >
                    <View style={{}}>
                      <TouchableWithoutFeedback
                        onPress={this.selectPhotoTapped.bind(this)}>
                        <Text style={styles.fabIcon}>+</Text>
                      </TouchableWithoutFeedback>
                    </View>
                  </View>
                </Card>
                </>
              )}

              <Button style={{marginTop:10,backgroundColor:'#B88A1C'}} onPress={this.handleSubmit} full><Text>Send Enquiry</Text></Button>
              </Form>
                    </View>
                </Content>
              }

            </Container>
      );
    }
}

const styles = StyleSheet.create({

    img: {
        height: 20, width: 20, backgroundColor: 'yellow'
    },
    header_img: {
        height: 90, width: 90
    },
    viewCardmap: {
        flexDirection: 'row',
        justifyContent: 'flex-start',
        alignContent: 'center',
        alignItems: 'center',
        margin: 10,
        flexWrap: 'wrap',
        display: 'flex',
        flex: 1
    },
    editStyle: {
      backgroundColor: '#fff',
      height:130,
      width:120,
      borderRadius:10
    },
    fabIcon: {
        alignSelf: 'center',
        fontSize: 70,
        color: '#ccc',
      },
     
});

export default withApollo(PostDemand)