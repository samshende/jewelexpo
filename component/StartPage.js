import React, { Component } from 'react';
import {
    StyleSheet,
    TouchableHighlight,
    Image,
    TouchableWithoutFeedback,
    AsyncStorage
} from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
    Container, Header, Content, Footer, FooterTab, Button, Icon, Text, View, Card, Left, Right, Thumbnail, Body
} from 'native-base';

class StartPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isConnected: true
        };
    }

    componentDidMount() {
        NetInfo.addEventListener(state => {
            console.log("Connection type", state.type);
            console.log("Is connected?", state.isConnected);
        });
        AsyncStorage.getItem('user')
            .then(data => {
                let data1 = JSON.parse(data);
                if (data1 != null) {
                    console.log("user", data1)
      this.props.navigation.navigate('Dashbord')
                }
                else {
                    this.props.navigation.navigate('Login')
                }
            })
            .catch(err => console.log('Error in login page', err));
    }

    componentWillMount() {
        NetInfo.fetch().then(isConnected => {
            this.setState({ isConnected });
        });
    }
   

    render() {
        console.log("props", this.props)
        if (!this.state.isConnected) {
            return <Text>></Text>;
        }
        return (
            <View
                style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center",
                    backgroundColor: '#eff1f5e0',
                }}>
                <Image
                    style={styles.header_img}
                    source={require('../Public/img/image.png')}
                />
            </View>

        );
    }
}

const styles = StyleSheet.create({

    header_img: {
        height: 300,
        width: 350,
        resizeMode: 'center'
    },

});

export default withApollo(StartPage)