import React, { Component } from 'react';
import {
  StyleSheet,
  AsyncStorage
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import { withApollo, compose, graphql } from 'react-apollo';
import Icon1 from 'react-native-vector-icons/AntDesign'
import getAppointmentsByVisitor from '../GraphQl/getAppointmentsByVisitor'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Spinner,
  Item, Input, Icon, Label, Textarea, Form, DatePicker, CheckBox, ListItem, Toast
} from 'native-base';

let arr = [1, 2, 3]

class BookedApp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isConnected: true,
      loading: false,
    };
  }



  componentDidMount() {
    NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
    });
    AsyncStorage.getItem('user')
      .then(data => {
        let data1 = JSON.parse(data);
        if (data1 != null) {
          console.log("user", data1)
          console.log("ss", data1.id)
          this.setState({
            id: data1.id,
          }, () => { this.fetchQuery(data1.id) })
        }
      })
      .catch(err => console.log('Error in login page', err));
  }

  fetchQuery = (id) => {
    console.log("state",id)
    try {
      this.props.client
        .query({
          query: getAppointmentsByVisitor,
          fetchPolicy: 'no-cache',
          variables: {visitorMob:'9665938860'},
        })
        .then(({ data }) => {
          console.log("data", data)
          this.setState({
            loading: false,
          })
        })
        .catch(err => {
          console.log(err);
        });
    } catch (err) {
      console.log('iN catfch', err);
    }

  }

  componentWillMount() {
    NetInfo.fetch().then(isConnected => {
      this.setState({ isConnected });
    });
  }


  render() {
    const minDate = new Date();
    if (!this.state.isConnected) {
      return <Text>></Text>;
    }

    return (
      <View style={{ flex: 1, alignContent: 'center', alignItems: 'center' }}>
        <Text>Booked Appointments</Text>
      </View>

      //  <Content>
      // <View style={{margin:10}}>
      //   {arr.map((i,j)=>{
      //     return(
      //       <Card style={{width:wp('95%'),height:'auto',borderRadius:10,alignSelf:'center'}}>
      //       <View style={{margin:7}}>
      //         <Text style={{textAlign:'right',right:5,top:5}}>10/07/2020</Text>
      //         <Text>Exhibitor Name:<Text style={{fontWeight:'bold'}}>Recaho</Text></Text>
      //         <Text>Company Name:<Text style={{fontWeight:'bold'}}>Recaho</Text></Text>
      //         <Text>First Name:<Text style={{fontWeight:'bold'}}>Sam</Text></Text>
      //         <Text>Last Name:<Text style={{fontWeight:'bold'}}>shende</Text></Text>
      //         <Text>Email:<Text style={{fontWeight:'bold'}}>ss@gmail.com</Text></Text>
      //         <Text>Mobile Number:<Text style={{fontWeight:'bold'}}>9665938860</Text></Text>
      //         <Text>Appointments Note:<Text style={{fontWeight:'bold'}}>new App</Text></Text>
      //         <Text>City:<Text style={{fontWeight:'bold'}}>pune</Text></Text>
      //       </View>
      //     </Card>
      //     )
      //   })}


      // </View>
      // </Content>

    );
  }
}

const styles = StyleSheet.create({

});


export default withApollo(BookedApp)