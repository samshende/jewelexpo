
import React, { Component } from 'react';
import {
  StyleSheet,
  TouchableHighlight,
  Image,
  Picker,
  Modal,
  AsyncStorage
} from 'react-native'
import NetInfo from "@react-native-community/netinfo";
import PhoneInput from 'react-native-phone-input'
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from 'react-native-responsive-screen';
import {
  Container, Header, Content, Footer, FooterTab, Button, Text, View, Card, Left, Right, Thumbnail, Body, Item, Input, Icon, Spinner, Label, Toast
} from 'native-base';
import { withApollo, compose, graphql } from 'react-apollo';
import generateOtp from '../GraphQl/generateOtp'
import verifyOtp from '../GraphQl/verifyOtp'
class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selectedValue: 'js',
      userMob: '',
      loading: true,
      isVerify: false,
      otp: '',
      isLoggedIn: false,
      inOtpProcess: false,
      unauth: false,
      otpSent: false,
      verifyOtpFailed: false,
      isConnected: true,
      inValidNumber: false,
      inValidName: false,
      inValidOtp: false,
      exhName: "jewExpo",
      num: '+91'
    };
  }




  componentDidMount() {
    NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
    });



  }

  componentWillMount() {
    NetInfo.fetch().then(isConnected => {
      this.setState({ isConnected });
    });
  }

  sendOtp = () => {
    if (this.state.num.length < 10) {
      this.setState({
        inValidNumber: true
      })
      return
    }
    this.setState({
      verifyOtpFailed: false,
      otpSent: false
    })
    let number = this.state.num.substr(-10)

    this.setState({
      inOtpProcess: true,
    });
    this.props.client
      .mutate({
        mutation: generateOtp,
        variables: {
          userMob: number
        },
      })
      .then(({ data }) => {
        console.log(data)
        this.setState({
          inOtpProcess: false,
          otpSent: true,
        });
      })
      .catch(err => {
        console.log(`generateOtp : in err: ${JSON.stringify(err)}`);
      });


  };

  renderRegister = (data) => {
    if (data && data.basicVisitor && data.basicVisitor.fName != null) {
      console.log(data)
      AsyncStorage.setItem('user', JSON.stringify(data));
      this.props.navigation.navigate('Dashbord')
    }
    else {
      console.log(data)
      this.props.navigation.navigate('Register', {
        mob: this.state.userMob,
      });
    }

  }

  verifyOtpCheck = () => {
    this.setState({
      inOtpProcess: true,
      inValidNumber: false
    });
    let number = this.state.num.substr(-10)
    const { userMob, otp, num } = this.state;
    this.props.client
      .mutate({
        mutation: verifyOtp,
        variables: {
          userMob: number,
          otp,
          exhName: this.state.exhName
        },
      })
      .then(({ data }) => {
        console.log(data)
        this.setState({
          inOtpProcess: false,
        });
        Toast.show({
          text: 'OTP has Verified successfully',
          type: 'success',
          position: 'center',
          duration: 2000,
          style:{alignSelf:'center',width:wp('70%'), height:"auto",borderRadius:10,marginTop:20},
          textStyle: {
            textAlign: 'center',
            color:'black'
          }
        });
        this.renderRegister(data.verifyOtp)
      })
      .catch(err => {
        Toast.show({
          text: 'Please Enter Correct OTP Number!',
          type: 'danger',
          position: 'center',
          duration: 5000,
          style:{alignSelf:'center',width:wp('70%'), height:"auto",borderRadius:10,marginTop:20},
          textStyle: {
            textAlign: 'center',
            color:'black'
          }
        });
        console.log(err)
        this.setState({
          verifyOtpFailed: true,
          inOtpProcess: false,
        });
      });
  };
  changeNum = (i) => {

    console.log('i', i)
    this.setState({
      num: i,
      inValidNumber: false,
    })
  }

  render() {
    if (!this.state.isConnected) {
      return <Text>></Text>;
    }
    console.log('opt', this.state.num)
    return (
      <Container style={{ backgroundColor: '#e5e5e5' }}>
        <Header style={{ backgroundColor: '#eff1f5e0', height: 80, elevation: 0 }}>
          <Thumbnail style={{ width: 180,height:70,alignSelf:'stretch',marginTop:5 }} square source={require('../Public/img/image.png')} />
        </Header>
        <Content>
          <View style={{}}>
            <Label style={{ marginTop: 7, marginBottom: 10, marginLeft: 15 }}>Enter mobile number</Label>
            <PhoneInput style={{ alignSelf: 'center', width: wp('91%'), height: 45, backgroundColor: 'white' }} value={this.state.num} onChangePhoneNumber={(i) => { this.changeNum(i) }} ref='phone' />

            {this.state.otpSent && (
              <Text style={{ color: 'green', marginLeft: 20, marginTop: 5 }}>
                OTP has been successfully sent...{' '}
              </Text>
            )}

            {this.state.inValidNumber && (
              <Text style={{ color: 'red', marginLeft: 20, marginTop: 5 }}>
                Please Enter valid Number...{' '}
              </Text>
            )}
            {this.state.otpSent && (
              <>
                <Label style={{ marginTop: 7, marginBottom: 10, marginLeft: 15 }}>Enter  OTP</Label>
                <Input
                  style={{ width: wp('91%'), backgroundColor: 'white', alignSelf: 'center' }}
                  pattern="[1-9]{1}[0-9]{9}"
                  placeholder=' OTP'
                  maxLength={4}
                  onChangeText={data =>
                    this.setState({
                      otp: data,
                    })
                  }
                />
              </>
            )}

            {this.state.verifyOtpFailed && (
              <Text style={{ color: 'red', marginLeft: 20 }}>
                Invalid Otp...{' '}
              </Text>
            )}
            {!this.state.otpSent && (
              <Button
                onPress={this.sendOtp}
                style={{ marginTop: 10, backgroundColor: '#B88A1C', width: wp('91%'), alignSelf: 'center' }}
                full >
                <Text>send otp</Text>
              </Button>
            )}
            {this.state.otpSent && (
              <Button
                full
                style={{ marginTop: 10, backgroundColor: '#B88A1C', width: wp('91%'), alignSelf: 'center' }}
                onPress={this.verifyOtpCheck}>
                <Text style={{ fontSize: 15 }}> Verify OTP</Text>
              </Button>
            )}
            {this.state.verifyOtpFailed && (
              <Button
                full
                style={{ marginTop: 10, backgroundColor: '#B88A1C', width: wp('91%'), alignSelf: 'center' }}
                onPress={this.sendOtp}>
                <Text style={{ fontSize: 15 }}> Resend OTP</Text>
              </Button>
            )}

            {this.state.inOtpProcess && <Spinner />}
          </View>
        </Content>
        <Footer style={{ backgroundColor: '#B88A1C' }}>
          <Text
            style={{ marginTop: 10, fontSize: 20, color: '#fff' }}>
            &#169;
              <Thumbnail
              small
              style={{ width: 100, height: 20 }}
              square
              source={require('../Public/img/recaho1.png')}
            />
          </Text>
        </Footer>
      </Container>
    );
  }
}

const styles = StyleSheet.create({

  img: {
    height: 20, width: 20, backgroundColor: 'yellow'
  },
  header_img: {

  },
  viewCardmap: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignContent: 'center',
    alignItems: 'center',
    margin: 10,
    flexWrap: 'wrap',
    display: 'flex',
    flex: 1
  },


});


export default withApollo(Login)
