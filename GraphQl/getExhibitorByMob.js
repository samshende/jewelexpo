import gql from 'graphql-tag';

export default gql`
  query getJewelExhibitorByMobForApp($mob: String!) {
    getJewelExhibitorByMobForApp(mob: $mob) {
      id
      companyName
      ownerName
      salesHead
      email
      compTel
      otherContact
      moreProductDetails
      target_states
      productsDealingIn
      productsMelting
      branchesIn
      slugCompName
      businessAddress {
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      latlng {
        lat
        lng
      }
      logo {
        bucket
        region
        key
      }
      jewExpo {
        siteId
        fqdn
        subdomain
        domain
        siteType
      }
    }
  }

  # {
  #   "mob": "8459832342"
  # }
`;