import gql from "graphql-tag";

export default gql`
  mutation requestSiteAccessToExhibitor(
    $contactPerson: String!
    $mob: String!
    $email: String!
    $city: String!
    $demanddetails: String!
    $demandphotos: [S3ObjectInput!]
  ) {
    requestSiteAccessToExhibitor(
      input: {
        contactPerson: $contactPerson
        mob: $mob
        email: $visitorEmail
        city: $city
        demanddetails: $demandDetails
        demandphotos: $demandPhotos
      }
    )
  }
`;
