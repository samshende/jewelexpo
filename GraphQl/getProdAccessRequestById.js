import gql from "graphql-tag";

export default gql`

query getProdAccessRequestById($exhId: String! $visitorId: String!){

  getProdAccessRequestById(exhId: $exhId visitorId: $visitorId){

    exhId

    visitorId

    name

    companyName

    city

    status

    createdAt

  }

}
`;
// # {

// #   "exhId": "8459832342",

// #   "visitorId": "9623111081"

// # }

