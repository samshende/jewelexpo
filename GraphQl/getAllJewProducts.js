import gql from 'graphql-tag'

export default gql`
query getAllJewProducts($limit: Int $nextToken: String){
    getAllJewProducts( limit: $limit nextToken: $nextToken){
      items{
        id
        exhId
        prodId
        caption
        category
        description
        enabled
        createdAt
        accessType
        image{
          bucket
          region
          key
        }
      }
      nextToken
    }
  }
  # {
  #   "limit": 50,
  #   "nextToken": ""
  # }`
  
  