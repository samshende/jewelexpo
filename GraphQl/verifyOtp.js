import gql from 'graphql-tag'

export default gql`
mutation verifyOtp($userMob: String! $otp: Int! $exhName: String!){
  verifyOtp(userMob: $userMob otp: $otp exhName: $exhName){
    id
    jewExpo{
      exhName
      productsInterestedIn
      status
      activatedOn
    }
    basicVisitor{
      fName
      lName
      companyName
      designation
      telephone
      email
      gender
      address{
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      visitorPhoto{
        bucket
        region
        key
      }
      docPhoto{
        bucket
        region
        key
      }
  }
    
  }
}


# {
#   "userMob": "8459832342",
#   "otp": 6349
# }
`