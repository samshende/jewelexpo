import gql from 'graphql-tag'

export default gql`
mutation createVisitor(
  $id: ID!
  $basicVisitor: CreateBasicVisitorInput
  $jewExpo: CreateJewExpoInput
  $exhName: String!
){
  createVisitor(input:{
    id: $id
    basicVisitor: $basicVisitor
    jewExpo: $jewExpo
    exhName: $exhName
  }){
    id
    jewExpo{
      exhName
      productsInterestedIn
      status
      activatedOn
    }
    basicVisitor{
      fName
      lName
      companyName
      designation
      telephone
      email
      gstNo
      countryCode
      gender
      address{
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      visitorPhoto{
        bucket
        region
        key
      }
      docPhoto{
        bucket
        region
        key
      }
  }
  }
}



# {
#   "id": "8459832342",
#   "basicVisitor": {
#     "fName": "sohel",
#     "lName": "khan",
#     "companyName": "recaho",
#   	"designation": "owner",
#     "telephone": "324343",
#     "email": "khan.sohel005@gmail.com",
#     "gender": "MALE",
#      "visitorPhoto": {
#     "bucket": "fd",
#     "region": "safsd",
#     "key": "fdf"
#   },
#     "address":{
#     "addressLineOne": "dange chowk",
#     "addressLineTwo": "chinchvad",
#     "zip": "32323",
#     "city": "pune",
#     "state": "MAHARASHTRA",
#     "city": "india",
#     "country": "india"
#   	},
#   "docPhoto": {
#      "bucket": "fd",
#     "region": "safsd",
#     "key": "fdf"
#   }
#   }
# }
`