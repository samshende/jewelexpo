import gql from 'graphql-tag'
export default gql`
  query getJewelExhibitors($exhName: String!) {
    getJewelExhibitors(exhName: $exhName) {
      id
      companyName
      ownerName
      salesHead
      email
      enabled
      compTel
      otherContact
      moreProductDetails
      target_states
      productsDealingIn
      productsMelting
      branchesIn
      slugCompName
      businessAddress {
        addressLineOne
        addressLineTwo
        city
        state
        zip
        country
      }
      latlng {
        lat
        lng
      }
      logo {
        bucket
        region
        key
      }
      marketingCollateral {
        caption
        doc {
          bucket
          region
          key
        }
      }
      jewExpo {
        siteId
        fqdn
        subdomain
        domain
        siteType
      }
    }
  }
`;