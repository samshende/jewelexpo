import gql from 'graphql-tag'

export default gql`
mutation createEnquiryToExhibitor(
  $userMob: String!,
  $salesHead: String!,
  $email: String!,
  $visitorCompanyName: String,
  $visitorName: String,
  $visitorMob: String,
  $visitorEmail: String,
  $enquiryDetails: String
){
  createEnquiryToExhibitor(
    input:{
      userMob: $userMob,
      salesHead: $salesHead,
    	email: $email,
      visitorCompanyName: $visitorCompanyName,
      visitorName: $visitorName,
      visitorMob: $visitorMob,
      visitorEmail: $visitorEmail,
      enquiryDetails: $enquiryDetails
    }
  )
}`