import gql from "graphql-tag";
export default gql`
mutation createAppointment(
  $userMob: String!
  $exhNumber: String!
  $exhCompany: String!
  $email: String
  $fName: String
  $lName: String
  $companyName: String
  $city: String
  $status: String
  $title: String!
  $notes: String
  $appDate: Int!
  $productInterestedIn: [PRODUCT_PROFILE]
){
  createAppointment(input:{
    userMob: $userMob
    exhNumber: $exhNumber
    email: $email
    fName: $fName
    lName: $lName
    companyName: $companyName
    city: $city
    status: $status
    title: $title
    notes: $notes
    appDate: $appDate
    exhCompany:$exhCompany
    productInterestedIn: $productInterestedIn
  })
}
`;