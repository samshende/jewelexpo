import gql from "graphql-tag";

export default gql`
mutation createProdAccessRequest(
  $exhId: String!
  $visitorId: String!
  $name: String
  $companyName: String
  $city: String
  $status: USER_STATUS!
){
  createProdAccessRequest(input:{
    exhId: $exhId
    visitorId: $visitorId
    name: $name
    companyName: $companyName
    city: $city
    status: $status
  }){
    exhId
    visitorId
    name
    companyName
    city
    status
    createdAt
  }

}
`;
// # {

// #   "exhId": "8459832342",

// #   "visitorId": "9623111081",

// #   "name": "shoaib",

// #   "companyName": "xyz",

// #   "city": "nanded",

// #   "status": "NEW"

// # }

