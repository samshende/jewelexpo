import gql from "graphql-tag";

export default gql`
  query getJewExhProductsByExhId($exhId: String!) {
    getJewExhProductsByExhId(exhId: $exhId) {
      id

      exhId

      prodId

      caption

      category

      description

      enabled

      createdAt

      accessType

      image {
        bucket

        region

        key
      }
    }
  }
`;

// # {

// #   "exhId": "8459832342"

// # }
