import gql from 'graphql-tag'

export default gql`
query getAppointmentsByVisitor($visitorMob: String!){
    getAppointmentsByVisitor(visitorMob: $visitorMob){
      id
      userMob
      exhNumber
      exhCompany
      email
      fName
      lName
      companyName
      city
      status
      title
      notes
      appDate
      # duration
      productInterestedIn
      eventId
    }
  }`