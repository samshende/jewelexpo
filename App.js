import React from 'react'
import { createStackNavigator, createAppContainer } from 'react-navigation';
import {ApolloProvider} from 'react-apollo';
import AWSAppSyncClient from 'aws-appsync';
import { Root } from "native-base";
import Dashbord from "./component/Dashbord"
import SearchProduct from './component/SearchProduct'
import Manufacturers from './component/Manufacturers'
import PostDemand from './component/PostDemand'
import BookAppointment from './component/BookAppointment'
import Notification from './component/Notification'
import Profile from './component/Profile'
import Login from './component/Login'
import Register from './component/Register'
import Enquiry from './component/Enquiry'
import ViewProduct from './component/viewProduct'
import Appointment from './component/Appointment'
import EnquiryImage from './component/EnquiryImage'
import ProtectedProducts from './component/protectedProducts'
import ViewExhibitor from './component/ViewExhibitor'
import StartPage from './component/StartPage'
import Approvedexhibitors from './component/Approvedexhibitors'
import configVariables from './config/AppSync'

const client = new AWSAppSyncClient({
  url: configVariables.graphqlEndpoint,
  region: configVariables.region,
  auth: {
    type: configVariables.authenticationType,
    apiKey: configVariables.apiKey,
  },
  disableOffline: true,
  complexObjectsCredentials: () => {
    return new AWS.Credentials({
      accessKeyId: configVariables.complexObjectsCredentialsAccessKeyId,
      secretAccessKey: configVariables.complexObjectsCredentialsSecretAccessKey,
    });
  },
});

console.log("cli",client)
const AppNavigator = createStackNavigator(
  {

    Dashbord: {
      screen: Dashbord, navigationOptions: {
        header: null,
      },
    },
    SearchProduct: {
      screen: SearchProduct, navigationOptions: {
        header: null,
      },
    },
    Manufacturers: {
      screen: Manufacturers, navigationOptions: {
        header: null,
      },
    },

    PostDemand: {
      screen: PostDemand, navigationOptions: {
        header: null,
      },
    },

    BookAppointment: {
      screen: BookAppointment, navigationOptions: {
        header: null,
      },
    },
    Notification: {
      screen: Notification, navigationOptions: {
        header: null,
      },
    },
    Profile: {
      screen: Profile, navigationOptions: {
        header: null,
      },
    },
    Login: {
      screen: Login, navigationOptions: {
        header: null,
      },
    },
    Register: {
      screen: Register, navigationOptions: {
        header: null,
      },
    },
    Enquiry: {
      screen: Enquiry, navigationOptions: {
        header: null,
      },
    },
    ViewProduct: {
      screen: ViewProduct, navigationOptions: {
        header: null,
      },
    },
    Appointment: {
      screen: Appointment, navigationOptions: {
        header: null,
      },
    },
    EnquiryImage: {
      screen: EnquiryImage, navigationOptions: {
        header: null,
      },
    },
    ProtectedProducts: {
      screen: ProtectedProducts, navigationOptions: {
        header: null,
      },
    },
    ViewExhibitor: {
      screen: ViewExhibitor, navigationOptions: {
        header: null,
      },
    },
    StartPage: {
      screen: StartPage, navigationOptions: {
        header: null,
      },
    },
    Approvedexhibitors: {
      screen: Approvedexhibitors, navigationOptions: {
        header: null,
      },
    },
  },

  {
    initialRouteName: 'StartPage',
    headerMode: 'none',
    mode: 'modal'
  }
);

const Name = (props) => (
  <ApolloProvider client={client}>
  <Root>
    <AppNavigator />
  </Root>
  </ApolloProvider>
);
export default Name;